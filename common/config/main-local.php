<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=112.213.89.171;dbname=moonesl_yii',
            'username' => 'moonesl_web',
            'password' => 'moonesl_123',
            /*'dsn' => 'mysql:host=localhost;dbname=esl',
           'username' => 'root',
           'password' => '',*/
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.moonesl.vn',
                'username' => 'admin@moonesl.vn',
                'password' => 'moonesl@123',
                'port' => '25'
            ],
        ],
    ],
];
