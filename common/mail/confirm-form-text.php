<?php

/* @var $this yii\web\View */
/* @var $form backend\models\Form */
/* @var $course backend\models\Courses */
?>
Chào bạn <?= $form->name ?>,
Bạn đã đăng ký thành công khóa học <?= $course-> name ?>.

Thông tin chi tiết như sau:
    Tên đăng nhập: <?= $form->name ?>
    Khóa học: <?= $course-> name ?>
    Học phí: <?= $course-> price ?>

Bạn hãy chuyển khoản hoặc hoặc nạp trực tiếp học phí để được chúng tôi kích hoạt trong vòng 24h.

Thông tin ngân hàng:
    Ngân hàng: TienPhongBank
    Tài khoản: 000423423423
    Chủ tài khoản: moonesl
    Chi nhanh: Hà nội
