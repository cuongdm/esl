<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $form backend\models\Form */
/* @var $course backend\models\Courses */
?>
<div class="password-reset">
    <p>Chào bạn <?= Html::encode($form->name) ?>,</p>

    <p>Bạn đã đăng ký thành công khóa học <?= Html::encode($course-> name) ?>.</p>

    <h6>Thông tin chi tiết như sau:<h6>
            <p>Tên đăng nhập: <?= Html::encode($form->name) ?></p>
            <p>Khóa học: <?= Html::encode($course-> name) ?></p>
            <p>Học phí: <?= Html::encode($course-> price) ?></p>

     <p>Bạn hãy chuyển khoản hoặc hoặc nạp trực tiếp học phí để được chúng tôi kích hoạt trong vòng 24h.<p>

     <h5>Thông tin ngân hàng:<h5>
         <p style="font-weight: bold">Ngân hàng: TienPhongBank</p>
         <p style="font-weight: bold">Tài khoản: 000423423423</p>
         <p style="font-weight: bold">Chủ tài khoản: moonesl</p>
         <p style="font-weight: bold">Chi nhanh: Hà nội</p>
</div>



