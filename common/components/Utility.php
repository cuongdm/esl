<?php

namespace common\components;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use backend\models\Question;
use yii\helpers\FileHelper;

class Utility extends Component
{
    public function genJsonFromStr($str){
        $split_array = explode("\n", $str);
        return $split_array;
    }
    
    public function updateQuestion($question){
        // if audio select
        if ($question->type == "5"){
            $question->answer_text = $this->getAnswerList($question->question_text);
        }
        if ($question->type == "6"){
            $question->answer_text = $question->question_text;
        }
    }
    
    public function moveCourseImage($course){
        $word = $course->image;
        if ($word == ""){
            return;
        }
        $final_img = "";
        $pos = strpos($word, "img/temp/");
        if ($pos !== false) {
            $directory = Yii::getAlias('@webroot/../../media/course') . DIRECTORY_SEPARATOR . $course->id . DIRECTORY_SEPARATOR;        
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            } 
            $filePath = $directory . basename($word);
            copy( Yii::getAlias('@backend/web/img/').$word, $filePath); 
            $final_img = Yii::$app->params['sub_folder']."/media/course/".$course->id ."/".basename($word);
        } 
        $course->image = $final_img;
    }
    
    public function textCut($strInp, $len){
        if (strlen($strInp)>$len){
            return substr($strInp, 0, $len)."...";
        } else {
            return $strInp;
        }
    }

    public function moveResource($question){
        // if audio select
        $words = explode("||",$question->question_audio);
        $final_audio = "";
        foreach($words as $word){
            if ($word == ""){
                continue;
            }
            $pos = strpos($word, "audio/temp/");
            if ($pos !== false) {
                $directory = Yii::getAlias('@webroot/../../media/audio') . DIRECTORY_SEPARATOR . $question->id . DIRECTORY_SEPARATOR;        
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                } 
                $filePath = $directory . basename($word);
                copy( Yii::getAlias('@backend/web/audio/').$word, $filePath); 
                $final_audio .= "||".Yii::$app->params['sub_folder']."/media/audio/".$question->id ."/".basename($word);
            }   
        }
        if ($final_audio != ""){
            $question->question_audio = trim($final_audio, "||");
        }
        $words = explode("||",$question->question_img);
        $final_img = "";
        foreach($words as $word){
            if ($word == ""){
                continue;
            }
            $pos = strpos($word, "img/temp/");
            if ($pos !== false) {
                $directory = Yii::getAlias('@webroot/../../media/img') . DIRECTORY_SEPARATOR . $question->id . DIRECTORY_SEPARATOR;        
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                } 
                $filePath = $directory . basename($word);
                copy( Yii::getAlias('@backend/web/img/').$word, $filePath); 
                $final_img .= "||".Yii::$app->params['sub_folder']."/media/img/".$question->id ."/".basename($word);
            } else{
                $final_img .= "||".$word;
            }
            
        }
        if ($final_img  != ""){            
            $question->question_img = trim($final_img, "||");
        }
    }
    
    public function getAnswerList($str){
        preg_match_all('/\|\|(.*?)\|\|/', $str, $matches);
        if (count($matches)>1){
            return json_encode($matches[1],JSON_FORCE_OBJECT);
        }
    }
}