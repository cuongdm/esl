<?php

namespace backend\modules\api\controllers;

use backend\models\Courses;
use backend\models\Form;
use Yii;
use yii\filters\Cors;
use yii\rest\ActiveController;

class CoursesController extends ActiveController
{
    
    public $modelClass = "backend\models\Courses";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                //'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['Origin', 'X-Requested-With', 'Content-Type', 'accept', 'Authorization'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['index','view','options'];

        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionForm()
    {
        $model = new Form();
        $model->scenario = Form::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $course = Courses::find()->where(['id'=>$model->course])->one();
            Yii::$app->mailer->compose(
                ['html' => 'confirm-form-html', 'text' => 'confirm-form-text'],
                ['form' => $model,'course'=>$course]
            )
                ->setFrom('vodanhbk172@gmail.com')
                ->setTo( $model->email_adress)
                ->setSubject('Moonesl: Register new course')
                ->send();

            return ["errorcode"=>"success","form" =>$model];
        } else {
            return ["errorcode"=>"fail","msg" =>json_encode($model->errors)];
        }
    }
}
