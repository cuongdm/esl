<?php

namespace backend\modules\api\controllers;

use backend\models\CourseKeyword;
use backend\models\CourseQuestionType;
use backend\models\Courses;
use backend\models\Keyword;
use backend\models\Question;
use backend\models\QuestionResult;
use backend\models\QuestionType;
use backend\models\Tree;
use backend\models\UserCourse;
use backend\modules\api\controllers\YoutubeGett\Extractor;
use backend\modules\api\controllers\YoutubeGett\VideoUrl;
use common\models\User;
use Yii;
use yii\authclient\clients\Google;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\authclient\signature\RsaSha;

class ExerciseController extends ActiveController
{
    public $modelClass = "backend\models\Courses";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                //'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['Origin', 'X-Requested-With', 'Content-Type', 'accept', 'authorization'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetUserCourse()
    {

        //get course_by_user
        $user_id = Yii::$app->user->identity->getId();
        //get data static
        $user_course = UserCourse::findAll(["user_id" => $user_id]);

        return ["usercourses" => $user_course];


        throw new ForbiddenHttpException();

    }

    public function actionGetDataByUserId()
    {

        //get course_by_user
        $user_id = Yii::$app->user->identity->getId();
        //get data static
        $post = Yii::$app->request->post();
        if (isset($post['course_id'])) {
            $course_id = $post['course_id'];
            $keyword = Keyword::find()->all();
            $question_type = QuestionType::find()->all();
            $CourseQuestionType = CourseQuestionType::find()->where(["course_id" => $course_id])->all();
            $CourseKeyword = CourseKeyword::find()->where(["course_id" => $course_id])->all();
            $Question = Question::find()->where(["course_id" => $course_id])->all();
            $QuestionResult = QuestionResult::find()->where(["question_id" => $this->extract_question_ids($Question)])->all();

            return ["keyword" => $keyword,
                "questiontype" => $question_type,
                "coursequestiontype" => $CourseQuestionType,
                "coursekeyword" => $CourseKeyword,
                "questions" => $Question,
                "questionresult" => $QuestionResult
            ];
        }


        throw new ForbiddenHttpException();

    }

    public function actionCreateQuestionResult()
    {
        $model = new QuestionResult();
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            return ["errorcode" => "success", "questionresult" => $model];
        } else {
            return ["errorcode" => "fail", "msg" => $model->errors];
        }

    }

    public function actionResetQuestionResult()
    {
        $model = new QuestionResult();
        $post = Yii::$app->request->post();
        $user_id = Yii::$app->user->identity->getId();
        if ($post && $user_id) {
            $questionTypeId = $post["question_type_id"];
            $questionId = Question::find()->where(["course_id" => $post["course_id"], "type" => $questionTypeId])->all();
            if ($questionId) {
                $model::deleteAll(array('AND', 'user_id = :user_id', array('IN', 'question_id', $this->extract_question_ids($questionId))), array(':user_id' => $user_id));
                if ($questionTypeId == 2 || $questionTypeId == 8)
                {
                    //deleted file record
                    foreach ($questionId as $k => $v) {
                        $path = $v-> question_audio;
                        $length = strpos($path,"/",1);
                        $esl  = substr($path,0,$length);
                        $path = str_replace($esl, '', $path);
                        $pathroot = \Yii::getAlias('@webroot') . "/../..";
                        unlink($pathroot . $path);
                    }
                }
                return ["errorcode" => "success"];
            }
        } else {
            return ["errorcode" => "fail", "msg" => $model->errors];
        }

    }

    public function actionUploadAudio()
    {
        $post = Yii::$app->request->post();
        if (!isset($post['audio-filename'])) {
            return ["errorcode" => "fail", "msg" => "không có file đính kèm"];
        }
        $fileName = '';
        $tempName = '';

        if (isset($_POST['audio-filename'])) {
            $fileName = $_POST['audio-filename'];
            $tempName = $_FILES['audio-blob']['tmp_name'];
        }
        if (empty($fileName) || empty($tempName)) {
            return ["errorcode" => "fail", "msg" => "không có file đính kèm"];
        }
        $path =  \Yii::getAlias('@webroot') . "/../..";
        $filePath = '/media/audio/' . $fileName;

        // make sure that one can upload only allowed audio/video files
        $allowed = array(
            'wav'
        );
        $extension = pathinfo($filePath, PATHINFO_EXTENSION);
        if (!$extension || empty($extension) || !in_array($extension, $allowed)) {
            return ["errorcode" => "fail", "msg" => "không có file đính kèm"];
        }

        if (!move_uploaded_file($tempName, $path . $filePath)) {
            return ["errorcode" => "fail", "msg" => "không có file đính kèm"];
        } else {
            //update link to question

            $model = new QuestionResult();
            $post['QuestionResult'] = json_decode($post['QuestionResult'], true);
            if ($model->load($post)) {
                $question = Question::find()->where(["id" => $model->question_id])->one();
                if ($question) {
                    $question->question_audio = "/esl" . $filePath;
                    if($question->save())
                    {
                        $model ->result = $question->question_audio;
                    }
                }
                if ($model->save()) {
                    return ["errorcode" => "success", "questionresult" => $model];
                }
            } else {
                return ["errorcode" => "fail", "msg" => "Không load được thông tin kết quả"];
            }
        }
    }

    public function actionGetLinkVideo()
    {
        $post = Yii::$app->request->post();
        if (isset($post['url'])) {
            $url = $post['url'];
            //$url = "https://drive.google.com/file/d/0B-rZP6g1y9FUNW1teS1HYmZWVUk/view?usp=sharing";
            $key = '70cd1724e702aec64f8171b1be17b7bd'; // Enter your key on VideoAPI.io
            $api = 'https://videoapi.io/api/getlink?key=' . $key . '&link=' . $url;
            $sources = $this->curl($api);
            $sourcesDecode = json_decode($sources);
            return ["errorcode" => "success", "src" => $sourcesDecode];
        }

        return ["errorcode" => "fail", "src" => ""];

    }

    /**
     * @return array
     */
    public function actionGetLinkTPlugin()
    {
        $post = Yii::$app->request->post();
        if (isset($post['id'])) {
            $question = Question::find()->where(['id' => $post['id']])->one();
            if ($question) {
                if (!empty($question->question_video)) {
                    $url = $question->question_video;
                    $data = array(
                        "key" => "670455c5f4939cab678452f49728c2d6",
                        "url" => $url,
                        "image" => "http://moonesl.vn/esl/media/img/moon-logo-01.jpg",
                        "download" => "no",
                        "ssl" => "no",
                        "html" => "no"
                    );
                    $param = http_build_query($data);
                    $player = file_get_contents("http://play.tplugin.com/play?" . $param);
                    return ["errorcode" => "success", "src" => $player];
                }
            }
        }
        return ["errorcode" => "fail", "src" => ""];
    }

    public function actionGetLinkVideoApi()
    {
        $post = Yii::$app->request->post();
        if (isset($post['id'])) {
            $question = Question::find()->where(['id' => $post['id']])->one();
            if ($question) {
                if (!empty($question->question_video)) {
                    $url = $question->question_video;
                    $data = array(
                        "hash"=>"71s1s7susv17rvt6v70217244w8170tu",
                        "url" => $url,
                        "download" => "false"
                    );

                    $param = http_build_query($data);
                    //$player = $this->curl("https://videoapi.io/api/getlink/embed?" . $param);
                    $player = $this->curl("https://videoapi.io/api/getlink/embed?hash=71s1s7susv17rvt6v70217244w8170tu&url=https://www.youtube.com/watch?v=yMuBSxSIS04");
                    return ["errorcode" => "success", "src" => $player];
                }
            }
        }
        return ["errorcode" => "fail", "src" => ""];
    }

    /**
     * @return array
     */
    public function actionGetLinkVideoGSuite()
    {
        $post = Yii::$app->request->post();
        if (isset($post['url'])) {
            $url = $post['url'];
            $token = $this->getAccessToken();
            $video_info = $this->curlWithToken($this->getDriveId($url), $token);
            $video_info = urldecode(urldecode($video_info));
            $s1 = 'fmt_stream_map=';
            $s2 = '&fmt_list';
            $i1 = strpos($video_info, $s1);
            $i2 = strpos($video_info, $s2);
            $t = substr($video_info, strlen($i1 . $s1), $i2);
            $t = mb_split('|', $t);
            $candidates = [];
            $check = [];
            for ($i = 0; $i < count($t); $i++) {
                $obj = $t[$i];
                if (strpos($obj, 'https') >= 0) {
                    $quantity = $this->getQuantiy($obj);
                    if (isset($quantity)) {
                        if (!$check[$quantity]) {
                            $m1 = strpos($obj, '/videoplay');
                            $domain = substr($obj, 0, $m1);
                            $obj = substr($obj, 0, strlen($t[$i]) - 3);
                            $link = str_replace($domain, 'https://redirector.googlevideo.com', $obj);
                            array_push($candidates, ["file" => $link, "label" => $quantity]);
                            $check[$quantity] = true;
                        }
                    }
                }

                return ["errorcode" => "success", "src" => $candidates];
            }
        }

        return ["errorcode" => "fail", "src" => ""];
    }

    public function actionGetVideoYoutube()
    {
        $post = Yii::$app->request->post();
        if (isset($post['id'])) {
            $question = Question::find()->where(['id' => $post['id']])->one();
            if ($question) {
                if (!empty($question->question_video)) {
                    $url = $question->question_video;
                    $id = $this->getIdYoutube($url);
                    $url = new VideoUrl($id);// "https://www.youtube.com/watch?v=ecIWPzGEbFc");
                    $v = new Extractor($url);
                    $return = array();

                    foreach($v->getVideoData()->getStreams()->isVideo()->asArray as $s)
                    {
                        //print("$s->type [$s->quality] Format: $s->itag\n");
                        //print($s->url."\n\n")
                        $m1 = strpos($s->url, '/videoplay');
                        $domain = substr($s->url, 0, $m1);
                        $link = str_replace($domain, 'https://redirector.googlevideo.com', $s->url);

                        $return[$s->type . "-" .$s->itag] = ["type"=>"video/mp4",
                            "label"=>$s->format->quality,
                            "default"=>"false",
                            "file"=>$link,
                            "src"=>$link
                        ];
                    }

                    return ["errorcode" => "success", "src" => array_values($return), "thumbnail" => $v->getVideoData()->thumbnail];
                }
            }
        }
    }

    function actionVideoYoutube2()
    {
        $post = Yii::$app->request->post();
        if (isset($post['id'])) {
            $question = Question::find()->where(['id' => $post['id']])->one();
            if ($question) {
                if (!empty($question->question_video)) {
                    $url = $question->question_video;
                    $id = $this->getIdYoutube($url);

                    //$getlink = "https://www.youtube.com/watch?v=" . $id;
                    $getlink = "http://www.youtube.com/get_video_info?video_id=" . $id . "&asv=3&el=detailpage&hl=en_US";
                    $token = $this->getAccessToken();
                    if ($get = $this->curlWithToken($getlink,$token)) {
                        $return = array();
                        $thumbnail = '';
                        parse_str($get, $output);

                        if (isset($output['reason'])) {
                            return $output['reason'];
                        }
                        if (isset($output['url_encoded_fmt_stream_map'])) {
                            $my_formats_array = explode(',', $output['url_encoded_fmt_stream_map']);
                        } else {
                            return 'No encoded format stream found.';
                        }
                        if (count($my_formats_array) == 0) {
                            return 'No format stream map found - was the video id correct?';
                        }

                        if (isset($output['thumbnail_url'])) {
                            $thumbnail = $output['thumbnail_url'];
                        }
                        foreach ($my_formats_array as $url) {
                            $url = str_replace('\u0026', '&', $url);
                            $url = urldecode($url);
                            parse_str($url, $data);
                            if (strpos($data['type'],'video/mp4') < 0)
                            {
                                continue;
                            }
                            $dataURL = $data['url'];

                            unset($data['url']);

                            $m1 = strpos($dataURL, '/videoplay');
                            $domain = substr($dataURL, 0, $m1);
                            $link = str_replace($domain, 'https://redirector.googlevideo.com', $dataURL);
                            $return[$data['quality'] . "-" . $data['itag']] = $link . '&' . urldecode(http_build_query($data));
                        }
                        return ["errorcode" => "success", "src" => array_values($return)[0], "thumbnail" => $thumbnail];
                    } else {
                        return 0;
                    }
                }
            }
        }
    }

    function getAccessToken()
    {
        $oauthClient = new Google();
        $oauthClient->scope = 'https://www.googleapis.com/auth/drive';
        $accessToken = $oauthClient->authenticateUserJwt(
            'hungle@video-private.iam.gserviceaccount.com',
            [
                'class' => RsaSha::className(),
                'algorithm' => OPENSSL_ALGO_SHA256,
                'privateCertificate' => "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDEYE5seA5Gf85h\nmtnzFrsKxpJExqpZkI689y4/P7/4000GQOUmcR4oEoG0t8VDEObhDh+u/43k/Opl\nAsdS9g5pCnQb9skr1n1FQ3vBpbu5TisdA1HkpDNZsg3VZlmFEYruHjAj+02gCCAN\nw+LNtLdF3oeZp+bYY4kR4D5ssqNPCRy+N10MaA/V3OBLfLqkAJAKaP8sNKadJBu+\nuGoyKw0U8N7VtdTWFlCd1SWwe4Ro9UYZjxRbfTkfEDjqUkwvVwV99eZ/N0tFabC5\nMXpTBCyu/jb+3Va3lyDANZWPSkTxsNwkb0EzAQ8DigUIi3LebwBTO2awDw/HhxNO\nGSewn/ErAgMBAAECgf9hj5IIXvskaZBo+t0/Z87wyC+fglb3DUzqNjQbRaTrAHrs\nO17gR8vH2Wnn+X+vh/LAcOxMupxEIKYcvsSma/ZefIjvW6UqXvLCirAS3GV7Utp2\nzCi0Mj3Xc7Jrt7em1xcM8mvCZxCpoAF1JhkVNGjmfIYXk+BYoOTPzZG8l1QspOy8\nBA+ctkydYcYiigKLwkLPTIyRP22/cXaSTRt4p97z1EggpqAMnnpFilKNn6xIfS97\nVXn96D65DWbIrxVxcO/7GSOvy6B3mbKqBsn825qNYweHVmIuBKLrqGT5uT6LN1P9\nc+wLrKBxDwr4ngUUs9Twe5TTNfs+DxOvcVD23N0CgYEA+caww+OPLFilxde3OWi2\n6Im/e2rReGxtApFWBkTVHjRYTjYVNRVVia+257YQO0ri8ucsOC09P5d8+zKb8amh\nlNlxgmOZcVxB510SIjOy7DEpBEJjlJKnF856uUqBLU0EGZLUptC3rnNMI6LEFlnz\nJ2OoQr+Qtml/BQiA4qePml0CgYEAyUT69aahBohKtsiHFJRe3B8vH0boZ+Hxap1A\ntny5MnXHLuBiq8w7gWPHjdOKxU5D6g8R2SQ1ONSusZKsgLjhOJlth/oqVHuHVBVT\n4MEN9Lbz3J6aq4t42oICKJgG/qIbJApxn3jej/MG0uBzbGFXa+eUNgSdx1HEdcI+\nshMIUScCgYEAmnr1lqHmSgIlvQAWtS37ekS/pgBBv54nqZ/juYrW2JB9URoH8cXZ\nGogPsq2aymEdjifF5JSuPedeiCob4tIMOnhlyMP7Ae6SYBri/T5GWehputWoqxos\nt/+fTXwz+qWYdIyocufBwuZSX/PqHPzS8ziit22tffQveBhzRjtmCI0CgYBJNu6T\nraml1q5XD1sUAWUAbdeWaRThtS88uTdxKq2zzWRHiRhbpk9/IezVir5QR8gsvI6h\njqpNohBqSBC4QoirLiTJ8haz5Lu1uoxoldGd02+iMY/SF0o/MkvWuHbS6eU1lxb+\nC9nqQ12YGE3kfwRuHC+t9r/uYbA1phNoc8PywQKBgQCtp6TtdqkfM9Ea4R1Y4scp\n9UamVa/SBwmoMD45jKkGgg+doSPk/pONPlDPKRl8EsvtI1p+uwgEmdNTZ2tVWD4R\nZtpa0Or0YyY929zfJB8k1VSXLPlWy4Z23JpBD0I4Yf4P9mYRWnRBepmESAI+4J4T\nkjQkz6oompesElEwkLKcqQ==\n-----END PRIVATE KEY-----\n"
            ]
        );

        return $accessToken->getToken();
    }

    function extract_course_ids($user_course)
    {
        $res = array();
        foreach ($user_course as $k => $v) {
            $res[] = $v->course_id;
        }
        return $res;
    }

    function extract_question_ids($questions)
    {
        $res = array();
        foreach ($questions as $k => $v) {
            $res[] = $v->id;
        }
        return $res;
    }

    // function curl
    function curl($url)
    {
        $ch = @curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $head[] = "Connection: keep-alive";
        $head[] = "Keep-Alive: 300";
        $head[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $head[] = "Accept-Language: en-us,en;q=0.5";
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $page = curl_exec($ch);
        curl_close($ch);
        return $page;
    }

    function curlWithToken($url, $token)
    {
        $ch = @curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $head[] = "Connection: keep-alive";
        $head[] = "Keep-Alive: 300";
        $head[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $head[] = "Accept-Language: en-us,en;q=0.5";
        $head[] = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6);
        $page = curl_exec($ch);
        curl_close($ch);
        return $page;
    }

    function getDriveId($url)
    {
        preg_match('/d\/(.+?)\//', $url, $match);
        if (isset($match[1])) {
            return 'https://docs.google.com/get_video_info?docid=' . $match[1];
        }
        return "";
    }

    function getIdYoutube($link)
    {
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $link, $id);
        if (!empty($id)) {
            return $id = $id[0];
        }
        return $link;
    }
}
