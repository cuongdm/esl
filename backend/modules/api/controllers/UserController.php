<?php

namespace backend\modules\api\controllers;

use backend\models\PasswordResetRequestForm;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use common\models\User;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class UserController extends ActiveController
{

    public $modelClass = "common\models\User";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                //'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['Origin', 'X-Requested-With', 'Content-Type', 'accept', 'Authorization'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];
        $behaviors['authenticator']['except'] = ['create', 'login', 'resetpassword','options'];

        return $behaviors;
    }

    public function actionLogin()
    {
        $post = \Yii::$app->request->post();
        $model = User::findOne(["username" => $post["username"]]);
        if (empty($model)) {
            return ['errorcode'=>'fail', 'msg'=> 'Tên người dùng không tồn tại'] ; //return whole user model including auth_key or you can just return $model["auth_key"];
        }
        if ($model->validatePassword($post["password"])) {
            $model->save(false);
            return ['errorcode'=>'success', 'user'=> $model] ; //return whole user model including auth_key or you can just return $model["auth_key"];
        } else {
            return ['errorcode'=>'fail', 'msg'=> 'Mật khẩu không đúng'] ; //return whole user model including auth_key or you can just return $model["auth_key"];
        }
    }

    public function actionUpdateFrontend($id)
    {
        $model = $this->findModel($id);
        $model->newPassword = $model->password_hash;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['errorcode'=>'success', 'user'=> $model] ; //return whole user model including auth_key or you can just return $model["auth_key"];
        } else {
            return ['errorcode'=>'fail', 'msg'=> json_encode($model->errors)] ; //return whole user model including auth_key or you can just return $model["auth_key"];
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionResetPassword()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                return ['errorcode'=>'success'] ; //return whole user model including auth_key or you can just return $model["auth_key"];
            } else {
                return ['errorcode'=>'fail', 'msg'=> 'Sorry, we are unable to reset password for the provided email address.'] ; //return whole user model including auth_key or you can just return $model["auth_key"];
            }
        }
        return ['errorcode'=>'fail', 'msg'=> 'Sorry, we are unable to reset password for the provided email address.'] ; //return whole user model including auth_key or you can just return $model["auth_key"];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
