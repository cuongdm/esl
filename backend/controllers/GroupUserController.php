<?php

namespace backend\controllers;

use Yii;
use backend\models\GroupUser;
use backend\models\CourseGroup;
use backend\models\Courses;
use backend\models\User;
use backend\models\GroupUserSearch;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GroupUserController implements the CRUD actions for GroupUser model.
 */
class GroupUserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GroupUser models.
     * @return mixed
     */
    public function actionIndex($group_id)
    {
        $searchModel = new GroupUserSearch();
        //$query = GroupUser::find();
        $query = new Query;
        $query->select(['group_user.id', 'group_user.group_id', 'user.username as user_id']) 
        ->from('group_user')
        ->leftJoin('user', 'user.id = group_user.user_id');

        $group = CourseGroup::findOne($group_id);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // grid filtering conditions
        $query->andFilterWhere([
            'group_id' => $group_id,
        ]);
        $this->layout = false;
        return $this->render('index', [
            "course_id" => $group->course_id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GroupUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GroupUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($group_id = '')
    {
        $model = new GroupUser();
        $model->group_id = $group_id;        
        $group = CourseGroup::findOne($group_id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $course = Courses::findOne($group->course_id);
            return $this->redirect(['courses/view', 'id' => $group->course_id, 'tag' => 'course_group']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'group_id' => $group_id,
                'course_id' => $group->course_id,
            ]);
        }
    }

    /**
     * Updates an existing GroupUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GroupUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->post('id');
        
        $group_user = $this->findModel($id);
        $group = CourseGroup::findOne($group_user->group_id);
        
        $group_user->delete();
        return $this->redirect(['courses/view', 'id' => $group->course_id, "tag" => "course_group"]);
    }

    /**
     * Finds the GroupUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GroupUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GroupUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
