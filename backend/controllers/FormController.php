<?php

namespace backend\controllers;

use Yii;
use backend\models\Form;
use backend\models\Courses;
use backend\models\User;
use backend\models\UserCourse;
use backend\models\FormSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use abhimanyu\sms\components\Sms;

/**
 * FormController implements the CRUD actions for Form model.
 */
class FormController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Form models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
   /**
     * Lists all Form models.
     * @return mixed
     */
    public function actionVerify($id)
    {
        $model = $this->findModel($id);
        $model->status = 1;
        $model->save();
        
        // create new user
        $user_model = User::find()
            ->where(['username' => $model->name])
            ->one();
        if ($user_model == null){
            $user_model = new User;
            $model->scenario = User::SCENARIO_CREATE;
            $user_model->username = $model->name;
            $user_model->email = $model->email_adress;
            $user_model->confirmation_token = null;
            $user_model->birthday = $model->birthday;
            $user_model->address = $model->address;
            $user_model->auth_key = Yii::$app->security->generateRandomString();
            $user_model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
            $user_model->updated_at =0;
            $user_model->created_at = 0;
            $user_model->avatarImage = "media/img/log.jpg";
            $user_model->type = "2";
            $user_model->status = 10;
            $user_model->save();

        }
        // assign to course
        $user_course = new UserCourse;
        $user_course->user_id = $user_model->id;
        $user_course->course_id = $model->course;
        $course = Courses::find()->where(['id'=>$model->course])->one();
        $user_course->duration = 30;
        if ($course->duration){
            $user_course->duration = $course->duration;
        }
        $user_course->start_date = date('Y-m-d H:i:s');
        $user_course->save();
        
        Yii::$app->mailer->compose()
        ->setFrom('vodanhbk172@gmail.com')
        ->setTo( $user_model->email)
        ->setSubject('Moonesl: account activated')
        ->setTextBody('your account activated: '.$user_model->username."\nyour new password: ".$model->password)
        ->setHtmlBody('your account activated: '.$user_model->username."\nyour new password: ".$model->password)
        ->send();
        
        $searchModel = new FormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }    

    /**
     * Displays a single Form model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Form model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Form();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Form model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->actionIndex();
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Form model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Form model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Form the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Form::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
