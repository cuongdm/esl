<?php

namespace backend\controllers;

use Yii;
use backend\models\Courses;
use backend\models\CoursesSearch;
use backend\models\SearchCourseQuestionType;
use backend\models\CourseKeywordSearch;
use backend\models\UserCourseSearch;
use backend\models\QuestionSearch;
use backend\models\CourseGroupSearch;
use yii\web\Controller;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;


/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoursesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
      // action examples

    public function actionUpload()
    {
        $model = new Courses();
        $post =Yii::$app->request->post();
        $model->load(Yii::$app->request->post());
        $imageFile = $_FILES["Courses"];
        $directory = Yii::getAlias('@backend/web/img/temp') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
        //return var_dump($directory);
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $imageFile["name"]["image"];
            $filePath = $directory . $fileName;
            copy($imageFile["tmp_name"]["image"], $filePath);            

            $path = '../img/temp/' . $fileName;
            return Json::encode([
                'files' => [
                    [
                        'name' => $fileName,
                        'size' => $imageFile["size"]["image"],
                        'url' => $path,
                        'thumbnailUrl' => $path,
                        'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                    ],
                ],
            ]);
            
        }

        return '';
    }
    

    /**
     * Displays a single Courses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($tag=null, $id)
    {
        $searchQuestionTypeModel = new SearchCourseQuestionType();
        $searchQuestionTypeModel->course_id = $id;
        $questionTypeDataProvider = $searchQuestionTypeModel->search(Yii::$app->request->queryParams);
        
        $searchKeywordModel = new CourseKeywordSearch();
        $searchKeywordModel->course_id = $id;
        $courseKeywordDataProvider = $searchKeywordModel->search(Yii::$app->request->queryParams);
        
        $searchQuestionModel= new QuestionSearch();
        $searchQuestionModel->course_id = $id;
        $dataQuestionProvider= $searchQuestionModel->search(Yii::$app->request->queryParams);
        
        $searchCourseUserModel= new UserCourseSearch();
        $searchCourseUserModel->course_id = $id;
        $CourseUserProvider= $searchCourseUserModel->search(Yii::$app->request->queryParams);

        $searchCourseGroupModel= new CourseGroupSearch();
        $searchCourseGroupModel->course_id = $id;
        $CourseGroupProvider= $searchCourseGroupModel->search(Yii::$app->request->queryParams);        

        return $this->render('view', [
            'model' => $this->findModel($id),
            'tag' => $tag,
            'searchQuestionTypeModel' => $searchQuestionTypeModel,
            'questionTypeDataProvider' => $questionTypeDataProvider,
            'searchKeywordModel' => $searchKeywordModel,
            'courseKeywordDataProvider' => $courseKeywordDataProvider,
            'dataQuestionProvider' => $dataQuestionProvider,
            'searchQuestionModel' => $searchQuestionModel,
            'CourseUserProvider' => $CourseUserProvider,
            'searchCourseUserModel' => $searchCourseUserModel,
            'CourseGroupProvider' => $CourseGroupProvider,
            'searchCourseGroupModel' => $searchCourseGroupModel,            
        ]);
    }

    /**
     * Creates a new Courses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Courses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            yii::$app->utility->moveCourseImage($model);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing Courses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image_text = $model->image;
        if ($model->load(Yii::$app->request->post()))
        {   
            if (is_null($model->image)||$model->image == ""){
                $model->image = $image_text;
            }   
            if ( $model->save()) {          
                yii::$app->utility->moveCourseImage($model);
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } 
        }else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Courses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Courses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Courses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
