<?php

namespace backend\controllers;

use Yii;
use backend\models\Question;
use backend\models\QuestionSearch;
use common\util\Utility;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use dosamigos\fileupload\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Json;
/**
 * QuestionController implements the CRUD actions for Question model.
 */
class QuestionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Question models.
     * @return mixed
     */
    public function actionIndex($tag,$course_id='')
    {
        $searchModel = new QuestionSearch();
        if ($tag == 'system') {
            $searchModel -> course_id = 0;
        }
        else
        {
            if ($course_id) {
               $searchModel -> course_id = $course_id;
            }
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tag' => $tag,
        ]);
    }

    // action examples

    public function actionUpload()
    {
        $model = new Question();
        $post =Yii::$app->request->post();
        $model->load(Yii::$app->request->post());
        $imageFile = $_FILES["Question"];
        $directory = Yii::getAlias('@backend/web/img/temp') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
        //return var_dump($directory);
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $imageFile["name"]["question_img"];
            $filePath = $directory . $fileName;
            copy($imageFile["tmp_name"]["question_img"], $filePath);            

            $path = '../img/temp/' . $fileName;
            return Json::encode([
                'files' => [
                    [
                        'name' => $fileName,
                        'size' => $imageFile["size"]["question_img"],
                        'url' => $path,
                        'thumbnailUrl' => $path,
                        'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                    ],
                ],
            ]);
            
        }

        return '';
    }
    
   // action examples

    public function actionImageDelete()
    {
        $get =Yii::$app->request->get();
        $imageFile = $get["name"];
        $directory = Yii::getAlias('@backend/web/img/temp') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
        //return var_dump($directory);
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $get["name"];
            $filePath = $directory . $fileName;           

            $path = '../img/temp/' . $fileName;
            return Json::encode([
                'files' => [
                    [
                        'name' => $fileName,
                        'url' => $path,
                        'thumbnailUrl' => $path,
                        'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                    ],
                ],
            ]);
            
        }

        return '';
    }    
// action examples

    public function actionUploadaudio()
    {
        $model = new Question();
        $post =Yii::$app->request->post();
        $model->load(Yii::$app->request->post());
        $imageFile = $_FILES["Question"];
        
        $directory = Yii::getAlias('@backend/web/audio/temp') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;        
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $imageFile["name"]["question_audio"];
            $filePath = $directory . $fileName;
            copy($imageFile["tmp_name"]["question_audio"], $filePath);            

            $path = '../audio/temp/' . $fileName;
            return Json::encode([
                'files' => [
                    [
                        'name' => $fileName,
                        'size' => $imageFile["size"]["question_audio"],
                        'url' => $path,
                        'thumbnailUrl' => '../img/audio.jpg',
                        'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                    ],
                ],
            ]);
            
        }

        return '';
    }
    

    /**
     * Displays a single Question model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($tag,$id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'tag' => $tag,
        ]);
    }

    /**
     * Creates a new Question model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($tag,$course_id = '')
    {
        $model = new Question();
        if ($tag=='system') {
            $model->course_id = 0;
        }else{
            $model->course_id = $course_id;
        }

        $portParam = Yii::$app->request->post();

        if ($portParam)
        {
            if (isset($portParam['QuestionSearch']))
            {
                //if had selection. post update
                if (!isset($portParam['selection']))
                    //filter of _importForm
                {
                    $searchModel = new QuestionSearch();
                    if ($tag!='system') {
                        $searchModel->course_id = $course_id;
                    }
                    $dataProvider = $searchModel->searchQuestionSystem($portParam);
                    return $this->render('create', [
                        'model' => $model,
                        'tag'=>$tag,
                        'createActive'=>false,
                        'importActive'=>true,
                        'searchModel'=>$searchModel,
                        'dataProvider'=>$dataProvider,
                    ]);
                }else
                {
                    for ($i = 0; $i < count($portParam['selection']);$i++)
                    {
                        //get from db
                        $modelClone = $this->findModel($portParam['selection'][$i]);
                        $modelNew = new Question();
                        $modelNew->attributes = $modelClone->attributes;
                        $modelNew->id = null;
                        $modelNew->course_id = $course_id;
                        $modelNew->save();
                    }
                    return $this->redirect(['courses/view','tag' =>$tag, 'id' => $model->course_id]);
                }
            }
            else
            { 
                if ($portParam['btn'] == 'import')
                {
                    //if had selection. post update
                    if (isset($portParam['selection']))
                    {
                        for ($i = 0; $i < count($portParam['selection']);$i++)
                        {
                            //get from db
                            $modelClone = $this->findModel($portParam['selection'][$i]);
                            $modelNew = new Question();
                            $modelNew->attributes = $modelClone->attributes;
                            $modelNew->id = null;
                            $modelNew->course_id = $course_id;
                            $modelNew->save();
                        }
                        return $this->redirect(['courses/view','tag' =>$tag, 'id' => $model->course_id]);
                    }
                    else
                    {
                        $searchModel = new QuestionSearch();
                        if ($tag!='system') {
                            $searchModel->course_id = $course_id;
                        }

                        $dataProvider = $searchModel->searchQuestionSystem($portParam);

                        Yii::$app->session->setFlash('error', 'Xin hãy chọn question muốn import.');
                        return $this->render('create', [
                            'model' => $model,
                            'tag'=>$tag,
                            'createActive'=>false,
                            'importActive'=>true,
                            'searchModel'=>$searchModel,
                            'dataProvider'=>$dataProvider,
                        ]);
                    }
                }else {
                    if ($model->load($portParam) ) {
                        yii::$app->utility->updateQuestion($model);
                        if ($model->save()){
                            yii::$app->utility->moveResource($model);
                            $model->save();
                            if ($tag == "system"){
                                return $this->redirect(['question/index','tag' =>$tag]);
                            }
                            return $this->redirect(['courses/view','tag' =>$tag, 'id' => $model->course_id]);
                        }
                    }else{

                        $searchModel = new QuestionSearch();
                        if ($tag!='system') {
                            $searchModel->course_id = $course_id;
                        }

                        $dataProvider = $searchModel->searchQuestionSystem($portParam);
                        return $this->render('create', [
                            'model' => $model,
                            'tag'=>$tag,
                            'createActive'=>true,
                            'importActive'=>false,
                            'searchModel'=>$searchModel,
                            'dataProvider'=>$dataProvider,
                        ]);
                    }
                }
            }
        }
        else {
            $searchModel = new QuestionSearch();
            if ($tag!='system') {
                $searchModel->course_id = $course_id;
            }
            $dataProvider = $searchModel->searchQuestionSystem(Yii::$app->request->queryParams);
            return $this->render('create', [
                'model' => $model,
                'tag'=>$tag,
                'createActive'=>true,
                'importActive'=>false,
                'searchModel'=>$searchModel,
                'dataProvider'=>$dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Question model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($tag,$id)
    {
        $model = $this->findModel($id);
        $audio_text = $model->question_audio;
        $image_text = $model->question_img;
        if ($model->load(Yii::$app->request->post())){  
            yii::$app->utility->updateQuestion($model);
            if (is_null($model->question_audio)||$model->question_audio == ""){
                $model->question_audio = $audio_text;
            }
            if (is_null($model->question_img)||$model->question_img == ""){
                $model->question_img = $image_text;
            }       
            if ($model->save()) {
                yii::$app->utility->moveResource($model);
                $model->save();
                if($model->course_id != -1){
                    return $this->redirect(['courses/view', 'id' => $model->course_id, 'tag'=>"question"]);
                }
                return $this->redirect(['view','tag'=>$tag, 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'tag'=>$tag,
            ]);
        }
    }

    
    /**
     * Deletes an existing Question model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$tag,$course_id = '')
    {
        $this->findModel($id)->delete();
        if ($tag == "course"){
            return $this->redirect(['courses/view','tag' =>"question", 'id' => $course_id]);
        }
        return $this->redirect(['index','tag' =>"system",]);
    }

    /**
     * Finds the Question model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Question the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Question::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
