<?php

namespace backend\controllers;

use Yii;
use backend\models\CourseKeyword;
use backend\models\CourseKeywordSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CourseKeywordController implements the CRUD actions for CourseKeyword model.
 */
class CourseKeywordController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseKeyword models.
     * @return mixed
     */
    public function actionIndex($tag, $course_id = '')
    {
        $searchModel = new CourseKeywordSearch();
        if ($tag == "system") {
            $searchModel->course_id = 0;
        } else {
            if ($course_id) {
                $searchModel->course_id =$course_id;
            }
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tag'=>$tag,
        ]);
    }

    /**
     * Displays a single CourseKeyword model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($tag, $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'tag'=>$tag
        ]);
    }

    /**
     * Creates a new CourseKeyword model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($tag,$course_id = '')
    {
        $model = new CourseKeyword();
        $model->scenario = CourseKeyword::SCENARIO_CREATE;
        if ($tag=='system') {
            $model->course_id = 0;
        }else{
            $model->course_id = $course_id;
        }

        $portParam = Yii::$app->request->post();

        if ($portParam)
        {
            if (isset($portParam['CourseKeywordSearch']))
            {
                //if had selection. post update
                if (!isset($portParam['selection']))
                //filter of _importForm
                {
                    $searchModel = new CourseKeywordSearch();
                    $searchModel->course_id = $course_id;

                    $dataProvider = $searchModel->searchKeywordSystem($portParam);
                    return $this->render('create', [
                        'model' => $model,
                        'tag'=>$tag,
                        'createActive'=>false,
                        'importActive'=>true,
                        'searchModel'=>$searchModel,
                        'dataProvider'=>$dataProvider,
                    ]);
                }else
                    {
                    for ($i = 0; $i < count($portParam['selection']);$i++)
                    {
                        $modelInsert = new CourseKeyword();
                        $modelInsert->course_id = $course_id;
                        $modelInsert->keyword = $portParam['selection'][$i];
                        $modelInsert->priority = $i;
                        $modelInsert->save();
                    }
                    return $this->redirect(['courses/view','tag' =>"keyword", 'id' => $model->course_id]);
                }
            }
            else
            {
                if ($portParam['btn'] == 'import')
                {
                        //if had selection. post update
                        if (isset($portParam['selection']))
                        {
                            $count = CourseKeyword::find()
                                    ->where(['course_id'=>$course_id])
                                    ->count();

                            for ($i = 0; $i < count($portParam['selection']);$i++)
                            {
                                $modelInsert = new CourseKeyword();
                                $modelInsert->course_id = $course_id;
                                $modelInsert->keyword = $portParam['selection'][$i];
                                $modelInsert->priority = $i + $count;
                                $modelInsert->save();
                            }
                            return $this->redirect(['courses/view','tag' =>"keyword", 'id' => $model->course_id]);
                        }
                        else
                        {
                            $searchModel = new CourseKeywordSearch();
                            $searchModel->course_id = $course_id;

                            $dataProvider = $searchModel->searchAllKeyword($portParam);
                            Yii::$app->session->setFlash('error', 'Xin hãy chọn keyword muốn import.');
                            return $this->render('create', [
                                'model' => $model,
                                'tag'=>$tag,
                                'createActive'=>false,
                                'importActive'=>true,
                                'searchModel'=>$searchModel,
                                'dataProvider'=>$dataProvider,
                            ]);
                        }
                }else {
                    if ($model->load($portParam) && $model->save()) {
                        return $this->redirect(['courses/view','tag' =>"keyword", 'id' => $model->course_id]);
                    }else{

                        $searchModel = new CourseKeywordSearch();
                        $searchModel->course_id = $course_id;

                        $dataProvider = $searchModel->searchKeywordSystem($portParam);
                        return $this->render('create', [
                            'model' => $model,
                            'tag'=>$tag,
                            'createActive'=>true,
                            'importActive'=>false,
                            'searchModel'=>$searchModel,
                            'dataProvider'=>$dataProvider,
                        ]);
                    }
                }
            }
        }
        else {
            $searchModel = new CourseKeywordSearch();
            $searchModel->course_id = $course_id;

            $dataProvider = $searchModel->searchAllKeyword(Yii::$app->request->queryParams);

            return $this->render('create', [
                'model' => $model,
                'tag'=>$tag,
                'createActive'=>true,
                'importActive'=>false,
                'searchModel'=>$searchModel,
                'dataProvider'=>$dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing CourseKeyword model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($tag, $id,$course_id = '')
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($tag == 'course')
            {
                return $this->redirect(['courses/view','tag'=>"keyword", 'id' => $course_id]);
            }
            else{
                return $this->redirect(['view','tag'=>$tag, 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'tag'=>$tag,
            ]);
        }
    }

    /**
     * Deletes an existing CourseKeyword model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$tag,$course_id='')
    {
        $this->findModel($id)->delete();
        if ($tag == "course")
        {
            return $this->redirect(['courses/view','id'=>$course_id ,'tag'=>"keyword"]);

        }else
        {
            return $this->redirect(['index','tag'=>$tag]);

        }
    }

    /**
     * Finds the CourseKeyword model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseKeyword the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseKeyword::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
