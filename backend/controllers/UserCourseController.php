<?php

namespace backend\controllers;

use Yii;
use backend\models\UserCourse;
use backend\models\UserCourseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserCourseController implements the CRUD actions for UserCourse model.
 */
class UserCourseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserCourse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserCourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserCourse model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserCourse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($userid)
    {
        $model = new UserCourse();
        $model->user_id = $userid;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['user/view', 'id' => $userid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserCourse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($tag, $id,$course_id = '')
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($tag == 'course')
            {
                return $this->redirect(['courses/view','tag'=>"user_course", 'id' => $course_id]);
            }
            else{
                return $this->redirect(['view','tag'=>$tag, 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'tag'=>$tag,
            ]);
        }
    }


    /**
     * Deletes an existing UserCourse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$tag,$course_id='')
    {
        $user_id = $this->findModel($id)->user_id;
        $this->findModel($id)->delete();
        if ($tag == "course")
        {
            if ($course_id){
                return $this->redirect(['courses/view','id'=>$course_id ,'tag'=>"user_course"]);
            } else {
                return $this->redirect(['user/view','id'=>$user_id]);
            }

        }else
        {
            return $this->redirect(['index','tag'=>$tag]);

        }
    }
    /**
     * Finds the UserCourse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserCourse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserCourse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
