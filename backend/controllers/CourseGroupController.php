<?php

namespace backend\controllers;

use Yii;
use backend\models\CourseGroup;
use backend\models\GroupUser;
use backend\models\CourseGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CourseGroupController implements the CRUD actions for CourseGroup model.
 */
class CourseGroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CourseGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CourseGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($course_id = '')
    {
        $model = new CourseGroup();
        $model->course_id = $course_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['courses/view', 'id' => $course_id, 'tag' => 'course_group']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'course_id' => $course_id,
            ]);
        }
    }

    /**
     * Updates an existing CourseGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CourseGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->post('id');
        
        $group = $this->findModel($id);
        $course_id = $group->course_id;
        
        $group->delete();
        GroupUser::deleteAll(['group_id' =>$group->id]);
        return $this->redirect(['courses/view', 'id' => $course_id, "tag" => "course_group"]);
    }

    /**
     * Finds the CourseGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
