<?php

namespace backend\controllers;

use Yii;
use backend\models\CourseQuestionType;
use backend\models\SearchCourseQuestionType;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CourseQuestionTypeController implements the CRUD actions for CourseQuestionType model.
 */
class CourseQuestionTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseQuestionType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchCourseQuestionType();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CourseQuestionType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CourseQuestionType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($course_id)
    {
        $model = new CourseQuestionType();
        $model->course_id = $course_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['courses/view', 'tag'=>'question_type', 'id' => $model->course_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'course_id' => $course_id,
            ]);
        }
    }

    /**
     * Updates an existing CourseQuestionType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($tag, $id,$course_id = '')
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($tag == 'course')
            {
                return $this->redirect(['courses/view','tag'=>"question_type", 'id' => $course_id]);
            }
            else{
                return $this->redirect(['view','tag'=>$tag, 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'tag'=>$tag,
            ]);
        }
    }

    /**
     * Deletes an existing CourseQuestionType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$tag,$course_id='')
    {
        $this->findModel($id)->delete();
        if ($tag == "course")
        {
            return $this->redirect(['courses/view','id'=>$course_id ,'tag'=>"question_type"]);

        }else
        {
            return $this->redirect(['index','tag'=>$tag]);

        }
    }

    /**
     * Finds the CourseQuestionType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseQuestionType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseQuestionType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
