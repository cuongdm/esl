<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Userstatuslookup;
use backend\models\Usertypelookup;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'confirmation_token',
            // 'status',
            [
                'attribute' => 'status',
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'value' => function ($data) {
                    $status = $data->status;
                    $statusObject = Userstatuslookup::find()->where(['id'=>$status])->One();
                    if ($statusObject) {
                        return $statusObject->name;
                    }
                    return $status;
                },
            ],
            // 'superadmin',
            // 'created_at',
            // 'updated_at',
            // 'registration_ip',
            // 'bind_to_ip',
             'email:email',
             'email_confirmed:email',
            // 'avatarImage',
             'birthday',
             'address:ntext',
             [
                'attribute' => 'type',
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'value' => function ($data) {
                    $type = $data->type;
                    $typeObject = Usertypelookup::find()->where(['id'=>$type])->One();
                    if ($typeObject) {
                        return $typeObject->name;
                    }
                    return $type;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
