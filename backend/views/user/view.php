<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\Courses;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'auth_key',
            'password_hash',
            'confirmation_token',
            'status',
            'superadmin',
            'created_at',
            'updated_at',
            'registration_ip',
            'bind_to_ip',
            'email:email',
            'email_confirmed:email',
            'avatarImage',
            'birthday',
            'address:ntext',
            'type',
        ],
    ]) ?>

</div>

<div class="user-course-index">
    <p>
        <?= Html::a('Assign Course', ['user-course/create','userid'=>$model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'course_id',
                'value' => function ($data) {
                    $course_id = $data->course_id;
                    $typeObject = Courses::find()->where(['id'=>$course_id])->One();
                    if ($typeObject) {
                        return $typeObject->name . '(' . $course_id . ')';
                    }
                    return $course_id;
                },
            ],
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{deleted}',
            'buttons' => [
            'deleted' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['user-course/delete','id'=>$model->id,'tag'=>'course'],['title'=>'delete',
                'class' => '',
                'data' => [
                    'confirm' => 'Bạn có chắc chắn muốn xóa khóa học ra khỏi người dùng.',
                    'method' => 'post',
                ]]);
            }
            ],
            ],
        ],
    ]); ?>
</div>

