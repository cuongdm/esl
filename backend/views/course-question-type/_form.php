<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\QuestionType;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\CourseQuestionType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-question-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'course_id')->textInput(['disabled' => 'true']) ?>

    <?= $form->field($model, 'question_type_id')->dropDownList(
        ArrayHelper::map(QuestionType::find()->all(), "id", "type_name"),
        ["prompt"=>"Select Question Type"]
    ) ?>
    <?= $form->field($model, 'priority')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
