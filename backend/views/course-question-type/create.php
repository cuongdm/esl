<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CourseQuestionType */

$this->title = 'Create Course Question Type';
$this->params['breadcrumbs'][] = ['label' => 'Course', 'url' => ['courses/view?tag=question_type&id='.$course_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-question-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
