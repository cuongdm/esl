<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="group-user-index">

   <p><h2>
        Danh sách học sinh của nhóm
        </h2>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'group_id',
            'user_id',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'additional_icon' => function ($url, $modelRow, $key) {
                        return Html::a ('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ', ['group-user/delete'], [
                                'data'=>[
                                    'method' => 'post',
                                    'params'=>[
                                        'id' => $modelRow["id"],
                                    ],
                                ]
                            ]);
                    },              
                ],
                'template' => '{view} {additional_icon}'

            ],
        ],
    ]); ?>
</div>
