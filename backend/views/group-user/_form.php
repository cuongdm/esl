<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;
use yii\db\Query;
/* @var $this yii\web\View */
/* @var $model app\models\GroupUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php

$userQuery = (new Query())->select('user_id')->from('user_course')->where(['course_id' => $course_id]);
$groupQuery = (new Query())->select('group_id')->from('course_group')->where(['course_id' => $course_id]);
$userExQuery = (new Query())->select('user_id')->from('group_user')->where(['group_id' => $groupQuery]);
$userRegQuery = (new Query())->select('user_id')->from('user_course')->where(['course_id' => $course_id]);
$userSearch = User::find()
->andWhere([ 'id' => $userQuery])
->andWhere([ 'not in', 'id', $userExQuery])
->all();
?>
<div class="group-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->dropDownList(
        ArrayHelper::map($userSearch, "id", "username"),
        ["prompt"=>"Select User"]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
