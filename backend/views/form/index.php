<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\dialog\Dialog;

echo Dialog::widget([
   'libName' => 'krajeeDialog',
   'options' => [], // default options
]);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Forms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email_adress:email',
            'mobile',            
            'created_at',
            'updated_at',
            //'registration_ip',
            'birthday',
            'address:ntext',
            'course',
            [
                'attribute'=>'status',
                'value'=>function ($model) {
                    if($model->status==0){
                        return 'new';
                    }
                    if($model->status==1){
                        return 'done';
                    }
                    if($model->status==2){
                        return 'in progress';
                    }
                },
                'filter'=>array("0"=>"new","1"=>"done")
            ],            
             ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'verify' => function ($url, $model, $key) {
                        //return Html::a('Create Team',Url::to('url'),['onclick'=>"$('#createTeamForm').show(500);$('#createTeamForm').css('display','')",'style'=>'margin-left: 30px;']);
                        //return Html::a ( '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> ', ['onclick'=>"$('#createTeamForm').show(500);$('#createTeamForm').css('display','')",'style'=>'margin-left: 30px;'] );
                        return Html::a('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> ', null, ['href' => 'javascript:verify('.$key.');']);
                    },
                ],
                //'template' => '{update} {view}'
                'template' => '{update} {view}'
            ],
            [
                'attribute'=>'status',
                'header'=>'Verify',
                'filter' => ['0'=>'New', '1'=>'Done'],
                'format'=>'raw',    
                'value' => function($model, $key, $index)
                {   
                    if($model->status == '0')
                    {
                        return Html::a('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> ', null, ['href' => 'javascript:verify('.$key.');']);
                    }
                    else
                    {   
                        return Html::a('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> ', null, ['href' => 'javascript:verify('.$key.');']);
                        return "";
                    }
                },
            ],
        ],
    ]); ?>
</div>
<script>
function verify(key){
    krajeeDialog.confirm('Are you sure', function(out){
        if(out) {
            window.location.replace("verify?id="+key);
        }
    }); 
}
</script>
