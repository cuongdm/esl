<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Courses;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\UserCourse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-course-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php $sql = "select id,name from courses where id not in (select course_id from user_course where user_id = :user_id)"; ?>
    <?= $form->field($model, 'course_id')->dropDownList(
        ArrayHelper::map(Courses::findBySql($sql,[':user_id'=>$model->user_id])->all()
            , "id", "name"),
        ["prompt"=>"Select Course"]
    ) ?>

       <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
        'language' => 'en',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
