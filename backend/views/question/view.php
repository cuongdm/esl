<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\QuestionType;
use backend\models\Keyword;

/* @var $this yii\web\View */
/* @var $model backend\models\Question */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index','tag' => $tag]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update','tag' => $tag, 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'template' => function($attribute, $index, $widget){
            //your code for rendering here. e.g.
            if($attribute['value'])
            {
                return "<tr><th>{$attribute['label']}</th><td>{$attribute['value']}</td></tr>";
            }
        },
        'attributes' => [
            'id',
             [
                'label'  => 'type',
                'format' => 'raw',
                'value'  => function ($data) {
                    return QuestionType::find(['type' => $data->type])->one()->type_name;
                },
            ],
            [
                'label'  => 'keyword',
                'format' => 'raw',
                'value'  => function ($data) {
                    return Keyword::find(['id' => $data->keyword])->one()->name;
                },
            ],            
            'question_text:ntext',
            'question_audio:ntext',
            [
                'attribute'=>'question_img',
                'value'=>$model->question_img,
                'format' => ['image',['width'=>'200']],
            ],
            [
                'format' => 'raw',
                'label'  => 'question_video',
                'value'  => call_user_func(function ($data) {
                    if ($data->question_video){
                        return  '<iframe width="420" height="315" src="'.$data->question_video.'" frameborder="0" allowfullscreen></iframe>';
                    }
                    return "";
                }, $model)
            ],
            'answer_text:ntext',
            [
                'attribute'=>'answer_img',
                'value'=>$model->answer_img,
                'format' => ['image',['width'=>'200']],
            ],
            'hint:ntext',
            'description:ntext',
            'time',
            //'course_id',
        ],
    ]) ?>

</div>
