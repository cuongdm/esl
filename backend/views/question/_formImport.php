<?php

use backend\models\CourseKeyword;
use backend\models\QuestionType;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseKeyword */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return ['value' => $model->id];
                }
            ],
            'id',
            [
                'attribute' => 'type',
                'value' => function ($data) {
                    $type = $data->type;
                    $questionType = QuestionType::find()->where(['id'=>$type])->One();
                    if ($questionType) {
                        return $questionType->type . '(' . $type . ')';
                    }
                    return $type;
                },
            ],
            [
                'attribute' => 'keyword',
                'value' => function ($data) {
                    $keyword = $data->keyword;
                    $courseKeyword = CourseKeyword::find()->where(['id'=>$keyword])->One();
                    if ($courseKeyword) {
                        return $courseKeyword->keyword . '(' . $keyword . ')';
                    }
                    return $keyword;
                },
            ],
             ['attribute' => 'question_text',
             'label' =>'question_text',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_text, 100);
                },           
             'contentOptions' => ['style' => 'width:300px; white-space: normal;'],
            ],
            ['attribute' => 'question_audio',
                'label' =>'question_audio',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_audio, 20);
                },
                'contentOptions' => ['style' => 'max-width:300px; white-space: normal;'],
            ],              
            ['attribute' => 'question_img',
                'label' =>'question_img',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_img, 20);
                },                
                 'contentOptions' => ['style' => 'max-width:300px; white-space: normal;'],
            ],  
            // 'question_img:ntext',
            // 'question_video:ntext',
            // 'answer_text:ntext',
            // 'answer_img:ntext',
            // 'hint:ntext',
            // 'description:ntext',
            // 'time',
            // 'course_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton('Import' , ['class' => 'btn btn-success','value'=>'import','name'=>'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
