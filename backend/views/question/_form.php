<?php

use backend\models\Keyword;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\QuestionType;
use backend\models\CourseKeyword;
use yii\helpers\ArrayHelper;
use mihaildev\ckeditor\CKEditor;
use dosamigos\fileupload\FileUpload;
use dosamigos\fileupload\FileUploadUI;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model backend\models\Question */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <?php $sql = "SELECT b.id,b.type_name as type FROM course_question_type as a INNER JOIN question_type as b ON a.question_type_id = b.id where course_id = :course_id";
     $sqlKeyword = "SELECT b.id,a.priority,b.name FROM course_keyword as a INNER JOIN keyword as b ON a.keyword = b.id where course_id = :course_id and lvl=2"; 
    if ($model->course_id == 0){
        $sql = "SELECT * from question_type" ;
        $sqlKeyword = "SELECT * from keyword WHERE lvl = 2"; 
    } 
    if ($model->course_id == 0){
        echo $form->field($model, 'type')->dropDownList(
            ArrayHelper::map(QuestionType::findBySql($sql)->all()
                , "id", "type"),
            ["prompt"=>"Select Question Type"]
        );
    } else {
        echo $form->field($model, 'type')->dropDownList(
            ArrayHelper::map(QuestionType::findBySql($sql,[':course_id'=>$model->course_id])->all()
                , "id", "type"),
            ["prompt"=>"Select Question Type"]
    ); 
    }?>
    
    <?php
    if ($model->course_id == 0){
        echo $form->field($model, 'keyword')->dropDownList(
        ArrayHelper::map(Keyword::findBySql($sqlKeyword)->orderBy('Priority')->all(), "id", "name"),
        ["prompt"=>"Select Keyword"]
    ); } 
    else 
{       echo $form->field($model, 'keyword')->dropDownList(
        ArrayHelper::map(Keyword::findBySql($sqlKeyword,[':course_id'=>$model->course_id])->orderBy('Priority')->all(), "id", "name"),
        ["prompt"=>"Select Keyword"]
    ) ; 
    }
    ?>
    
    <!--?=$form->field($model, 'question_text')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
            'rows' => ['rows' => 6],
        ],
    ]);
    ?-->
    <?= $form->field($model, 'question_text')->textarea(['rows' => 3]) ?>
    <!--?= $form->field($model, 'question_audio')->textInput() ?-->
    <div class="field-question-question_audio">
       <label for="question-image" class="control-label">Question_audio</label>
       <!--?= 
       $form->field($model, 'question_img')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
        ]);
   ?-->
       <?= FileUploadUI::widget([
        'model' => $model,
        'attribute' => 'question_audio',
        'value' => 1,
        'url' => ['question/uploadaudio', 'id' => $model->id],
        'gallery' => false,
        'fieldOptions' => [
            'accept' => 'audio/*'
        ],
        'clientOptions' => [
            'maxFileSize' => 20000000
        ],
        // ...
        'clientEvents' => [
            'fileuploaddone' => 'function(e, data) {
                                    var audio_url = $(\'input[type="hidden"][name="Question[question_audio]"]\').val();
                                    $(\'input[type="hidden"][name="Question[question_audio]"]\').val(audio_url + \'||\' + data.jqXHR.responseJSON.files[0].url);
                                }',
            'fileuploadfail' => 'function(e, data) {
                                }',
        ],
    ]); ?>
    </div>
   <div class="field-question-question_image">
       <label for="question-image" class="control-label">Question_image</label>
       <!--?= 
       $form->field($model, 'question_img')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
        ]);
   ?-->
       <?= FileUploadUI::widget([
        'model' => $model,
        'attribute' => 'question_img',
        'url' => ['question/upload', 'id' => $model->id],
        'gallery' => false,
        'fieldOptions' => [
            'accept' => 'image/*'
        ],
        'clientOptions' => [
            'maxFileSize' => 6000000
        ],
        // ...
        'clientEvents' => [
            'fileuploaddone' => 'function(e, data) {                                    
                                    var img_url = $(\'input[type="hidden"][name="Question[question_img]"]\').val();
                                    $(\'input[type="hidden"][name="Question[question_img]"]\').val(img_url + \'||\' + data.jqXHR.responseJSON.files[0].url);
                                }',
            'fileuploadfail' => 'function(e, data) {
                                }',
            'fileuploaddrop' => 'function(e, data) {
                                var img_url = $(\'input[type="hidden"][name="Question[question_img]"]\').val();
                                img_url = img_url.replace(data.files[0].url, "");
                                img_url = img_url.replace("||||", "||");
                                $(\'input[type="hidden"][name="Question[question_img]"]\').val(img_url);
                                }',               
        ],
    ]); ?>
    </div>
    <!--?= $form->field($model, 'question_img')->textarea(['rows' => 6]) ?-->

    <?= $form->field($model, 'question_video')->textInput() ?>

    <?= $form->field($model, 'answer_text')->textInput() ?>

    <?= $form->field($model, 'answer_img')->textInput() ?>

    <?= $form->field($model, 'hint')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name'=>'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
