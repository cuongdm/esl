<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Question */

$this->title = 'Create Question';
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index','tag' => $tag]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($tag == 'system') { ?>
        <?= $this->render('_form', [
            'model' => $model,
            'tag' => $tag,
        ]) ?>
    <?php } ?>
    <?php if ($tag == 'course') { ?>
        <?= Tabs::widget([
            'items' => [
                [
                    'label' => 'Tạo mới question cho khóa học',
                    'content' => $this->render('_form', [
                        'model' => $model,
                        'tag' => $tag,
                    ]),
                    'active' => $createActive
                ],
                [
                    'label' => 'Import question từ hệ thống',
                    'content' => $this->render('_formImport', [
                        'model' => $model,
                        'tag' => $tag,
                        'searchModel'=>$searchModel,
                        'dataProvider'=>$dataProvider,
                    ]),
                    'active'=>$importActive

                ]
            ]
        ]);
        ?>
    <?php } ?>
</div>

