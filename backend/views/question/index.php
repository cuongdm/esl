<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Courses;
use backend\models\QuestionType;
use backend\models\Keyword;
use backend\models\CourseKeyword;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\QuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Question', ['create','tag'=>$tag], ['class' => 'btn btn-success']) ?>
    </p>
    <?php if ($tag == 'system') { ?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id',
             'contentOptions' => ['style' => 'max-width:10px; white-space: normal;'],
            ],
            [
                'attribute' => 'type',
                'value' => function ($data) {
                    $type = $data->type;
                    $questionType = QuestionType::find()->where(['id'=>$type])->One();
                    if ($questionType) {
                        return $questionType->type . '(' . $type . ')';
                    }
                    return $type;
                },
                'filter'=>ArrayHelper::map(QuestionType::find()->asArray()->all(), 'id', 'type_name'),
            ],
            [
                'attribute' => 'keyword',
                'value' => function ($data) {
                    $keywordId = $data->keyword;
                    $keyword = Keyword::find()->where(['id'=>$keywordId])->One();
                    if ($keyword) {
                        return $keyword->name . '(' . $keywordId . ')';
                    }
                    return $keywordId;
                },
                'filter'=>ArrayHelper::map(Keyword::find()->asArray()->all(), 'id', 'name'),
            ],
            ['attribute' => 'question_text',
             'label' =>'question_text',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_text, 100);
                },           
             'contentOptions' => ['style' => 'width:300px; white-space: normal;'],
            ],
            ['attribute' => 'question_audio',
                'label' =>'question_audio',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_audio, 20);
                },
                'contentOptions' => ['style' => 'max-width:300px; white-space: normal;'],
            ],              
            ['attribute' => 'question_img',
                'label' =>'question_img',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_img, 20);
                },                
                 'contentOptions' => ['style' => 'max-width:300px; white-space: normal;'],
            ],            
            // 'question_video:ntext',
            // 'answer_text:ntext',
            // 'answer_img:ntext',
            // 'hint:ntext',
            // 'description:ntext',
            // 'time',
            // 'course_id',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{updated} {deleted}',
                'buttons' => [
                    'view' => function ($url, $model)  {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['question/view','id'=>$model->id,'tag'=>"system"],[
                            'title'=>'delete',
                            'class' => '',
                            'data' => [
                                'method' => 'post',
                            ]]);
                    },
                    'deleted' => function ($url, $model)  {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['question/delete','id'=>$model->id,'tag'=>"system"],[
                            'title'=>'delete',
                            'class' => '',
                            'data' => [
                                'confirm' => 'Bạn có chắc muốn xóa question này.',
                                'method' => 'post',
                            ]]);
                    },
                    'updated' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['question/update','id'=>$model->id,'tag'=>"system"],[
                            'title'=>'update',
                            'class' => '',
                            'data' => [
                                'method' => 'post',
                            ]]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php } ?>
    <?php if ($tag == 'course') { ?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'course_id',
                'value' => function ($data) {
                    $course_id = $data->course_id;
                    $course = Courses::find()->where(['id'=>$course_id])->One();
                    if ($course) {
                        return $course->name . '(' . $course_id . ')';
                    }
                    return $course_id;
                },
            ],
            [
                'attribute' => 'type',
                'value' => function ($data) {
                    $type = $data->type;
                    $questionType = QuestionType::find()->where(['id'=>$type])->One();
                    if ($questionType) {
                        return $questionType->type . '(' . $type . ')';
                    }
                    return $type;
                },
            ],
            [
                'attribute' => 'keyword',
                'value' => function ($data) {
                    $keyword = $data->keyword;
                    $courseKeyword = CourseKeyword::find()->where(['id'=>$keyword])->One();
                    if ($courseKeyword) {
                        return $courseKeyword->keyword . '(' . $keyword . ')';
                    }
                    return $keyword;
                },
            ],
            'question_text:ntext',
            'question_audio:ntext',
            // 'question_img:ntext',
            // 'question_video:ntext',
            // 'answer_text:ntext',
            // 'answer_img:ntext',
            // 'hint:ntext',
            // 'description:ntext',
            // 'time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php } ?>
</div>
