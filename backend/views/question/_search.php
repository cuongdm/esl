<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\QuestionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'keyword') ?>

    <?= $form->field($model, 'question_text') ?>

    <?= $form->field($model, 'question_audio') ?>

    <?php // echo $form->field($model, 'question_img') ?>

    <?php // echo $form->field($model, 'question_video') ?>

    <?php // echo $form->field($model, 'answer_text') ?>

    <?php // echo $form->field($model, 'answer_img') ?>

    <?php // echo $form->field($model, 'hint') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'time') ?>

    <?php // echo $form->field($model, 'course_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
