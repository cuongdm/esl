<?php

use backend\models\Courses;
use backend\models\Coursestatuslookup;
use backend\models\Keyword;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\QuestionType;
use backend\models\Question;
use backend\models\CourseKeyword;
use yii\bootstrap\Tabs;
use yii\grid\GridView;
?>
<br>

<div class="question-index">
    <p>
        <?= Html::a('Create Question', ['question/create','tag'=>'course','course_id'=>$model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataQuestionProvider,
        'filterModel' => $searchQuestionModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'course_id',
                'value' => function ($data) {
                    $course_id = $data->course_id;
                    $course = Courses::find()->where(['id'=>$course_id])->One();
                    if ($course) {
                        return $course->name . '(' . $course_id . ')';
                    }
                    return $course_id;
                },
            ],
            [
                'attribute' => 'type',
                'value' => function ($data) {
                    $type = $data->type;
                    $questionType = QuestionType::find()->where(['id'=>$type])->One();
                    if ($questionType) {
                        return $questionType->type . '(' . $type . ')';
                    }
                    return $type;
                },
            ],
            [
                'attribute' => 'keyword',
                'value' => function ($data) {
                    $keywordId = $data->keyword;
                    $keyword = Keyword::find()->where(['id'=>$keywordId])->One();
                    if ($keyword) {
                        return $keyword->name . '(' . $keywordId . ')';
                    }
                    return $keywordId;
                },
            ],
             ['attribute' => 'question_text',
             'label' =>'question_text',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_text, 100);
                },           
             'contentOptions' => ['style' => 'width:300px; white-space: normal;'],
            ],
            ['attribute' => 'question_audio',
                'label' =>'question_audio',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_audio, 20);
                },
                'contentOptions' => ['style' => 'max-width:300px; white-space: normal;'],
            ],              
            ['attribute' => 'question_img',
                'label' =>'question_img',
                'value' => function ($model) {
                    return yii::$app->utility->textCut($model->question_img, 20);
                },                
                 'contentOptions' => ['style' => 'max-width:300px; white-space: normal;'],
            ],  
            // 'question_img:ntext',
            // 'question_video:ntext',
            // 'answer_text:ntext',
            // 'answer_img:ntext',
            // 'hint:ntext',
            // 'description:ntext',
            // 'time',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{updated} {deleted}',
                'buttons' => [
                    'deleted' => function ($url, $model) use ($course_id) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['question/delete','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                            'title'=>'delete',
                            'class' => '',
                            'data' => [
                                'confirm' => 'Bạn có chắc chắn muốn xóa question.',
                                'method' => 'post',
                            ]]);
                    },
                    'updated' => function ($url, $model) use ($course_id) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['question/update','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                            'title'=>'update',
                            'class' => '',
                            'data' => [
                                'method' => 'post',
                            ]]);
                    }
                ],],
        ],
    ]); ?>
</div>