<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Course Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Course Group', ['course-group/create','course_id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $CourseGroupProvider,
        'filterModel' => $searchCourseGroupModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'course_id',
            ['class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'additional_icon' => function ($url, $modelRow, $key) {
                    return Html::a ('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ', ['course-group/delete'], [
                            'data'=>[
                                'method' => 'post',
                                'params'=>[
                                    'id' => $modelRow->id,
                                ],
                            ]
                        ]);
                },
                'update' => function ($url, $modelRow, $key) {
                    return Html::a ('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> ', ['group-user/create','group_id'=>$modelRow->id]);
                }, 
                'view'=>function ($url, $modelRow) {
                            $t = Yii::getAlias('@web').'/group-user/index?group_id='.$modelRow->id;
                            return Html::button('<span class="glyphicon glyphicon-eye-open"></span>', ['value'=>($t), 'class' => 'btn btn-default btn-xs custom_button']);
                        },                
            ],
            'template' => '{update} {view} {additional_icon}'


        ],
        ],
    ]); ?>
    <div id="group-user">
    </div>
</div>
