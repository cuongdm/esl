<?php

use backend\models\Courses;
use backend\models\Coursestatuslookup;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\QuestionType;
use backend\models\Question;
use backend\models\CourseKeyword;
use yii\bootstrap\Tabs;
use yii\grid\GridView;
?>
<br>
<div class="courses-index">

    <?= GridView::widget([
        'dataProvider' => $CourseUserProvider,
        //'filterModel' => $searchCourseUserModel,
        'columns' => [
             ['class' => 'yii\grid\SerialColumn'],
              'user.username',
              'start_date',
              'duration',
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{updated} {deleted}',
                    'buttons' => [
                        'view' => function ($url, $model) use ($course_id) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['user/view','id'=>$model->user_id],[
                                'title'=>'view user',
                                'class' => '',
                                'data' => [
                                    'method' => 'post',
                                ]]);
                        },
                        'deleted' => function ($url, $model) use ($course_id) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['user-course/delete','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                                'title'=>'delete',
                                'class' => '',
                                'data' => [
                                    'confirm' => 'Bạn có chắc muốn xóa user này khỏi khóa học.',
                                    'method' => 'post',
                                ]]);
                        },
                        'updated' => function ($url, $model) use ($course_id) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['user-course/update','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                                'title'=>'update',
                                'class' => '',
                                'data' => [
                                    'method' => 'post',
                                ]]);
                        }
                    ],
                ],
            
        ],
    ]); ?>
</div>
