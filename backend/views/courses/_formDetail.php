<?php

use backend\models\Courses;
use backend\models\Coursestatuslookup;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\QuestionType;
use backend\models\Question;
use backend\models\CourseKeyword;
use yii\bootstrap\Tabs;
?>
<br>
<div class="courses-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'price',
            'description:ntext',
            'image',
            'created_at',
            'percent',
            [
                'label'  => 'Status',
                'value'  => call_user_func(function ($data) {
                    $status = $data->status;
                    $typeObject = Coursestatuslookup::find()->where(['id'=>$status])->One();
                    if ($typeObject) {
                        return $typeObject->name;
                    }
                    return $status;
                }, $model)
            ],
            'duration',
        ],
    ]) ?>

</div>