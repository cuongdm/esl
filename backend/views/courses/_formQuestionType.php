<?php

use backend\models\Courses;
use backend\models\Coursestatuslookup;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\QuestionType;
use backend\models\Question;
use backend\models\CourseKeyword;
use yii\bootstrap\Tabs;
use yii\grid\GridView;
?>
<br>
<div class="courses-index">

    <p>
        <?= Html::a('Create Question Type For Course', ['course-question-type/create','tag'=>'question_type','course_id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $questionTypeDataProvider,
        'filterModel' => $searchQuestionTypeModel,
        'columns' => [
             ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'question_type_id',
                'value' => function ($data) {
                    $question_type_id = $data->question_type_id;
                    $typeObject= QuestionType::find()->where(['id'=>$question_type_id])->One();
                    if ($typeObject) {
                        return $typeObject->type . '(' . $question_type_id . ')';
                    }
                    return $question_type_id;
                },
            ],
            [
                'attribute' => 'question_type_id',
                'label' => 'Question Type Name',
                'value' => function ($data) {
                    $question_type_id = $data->question_type_id;
                    $typeObject= QuestionType::find()->where(['id'=>$question_type_id])->One();
                    if ($typeObject) {
                        return $typeObject->type_name;
                    }
                    return $question_type_id;
                },
            ],
            'priority',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{updated} {deleted}',
                'buttons' => [
                    'deleted' => function ($url, $model) use ($course_id) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['course-question-type/delete','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                            'title'=>'delete',
                            'class' => '',
                            'data' => [
                                'confirm' => 'Bạn có chắc muốn xóa question type.',
                                'method' => 'post',
                            ]]);
                    },
                    'updated' => function ($url, $model) use ($course_id) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['course-question-type/update','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                            'title'=>'update',
                            'class' => '',
                            'data' => [
                                'method' => 'post',
                            ]]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
