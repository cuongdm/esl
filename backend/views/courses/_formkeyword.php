<?php

use backend\models\Courses;
use backend\models\Coursestatuslookup;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\QuestionType;
use backend\models\Question;
use backend\models\CourseKeyword;
use yii\bootstrap\Tabs;
use yii\grid\GridView;
?>
<br>
<div class="courses-index">

    <p>
        <?= Html::a('Create Keyword For Course', ['course-keyword/create', 'tag'=>'course' , 'course_id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $courseKeywordDataProvider,
        'filterModel' => $searchKeywordModel,
        'columns' => [
             ['class' => 'yii\grid\SerialColumn'],
            'id',
            'keyword:ntext',
            'priority',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{updated} {deleted}',
                'buttons' => [
                    'deleted' => function ($url, $model) use ($course_id) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['course-keyword/delete','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                            'title'=>'delete',
                            'class' => '',
                            'data' => [
                                'confirm' => 'Bạn có chắc muốn xóa keyword.',
                                'method' => 'post',
                            ]]);
                    },
                    'updated' => function ($url, $model) use ($course_id) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['course-keyword/update','id'=>$model->id,'tag'=>"course",'course_id'=>$course_id],[
                            'title'=>'update',
                            'class' => '',
                            'data' => [
                                'method' => 'post',
                            ]]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>