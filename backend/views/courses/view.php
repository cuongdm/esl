<?php

use backend\models\Courses;
use backend\models\Coursestatuslookup;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\QuestionType;
use backend\models\Question;
use backend\models\CourseKeyword;
use yii\bootstrap\Tabs;

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Courses */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $course_id = $model->id ?>

<?php 
$tag1 = true;
$tag2 = $tag3 = $tag4 = $tag5= $tag6=false;
if(isset($tag)){    
    $tag1 = false;
    if ($tag == "keyword"){
        $tag2 = true;
    }
    if ($tag == "question_type"){
        $tag3 = true;
    }   
    if ($tag == "question"){
        $tag4 = true;
    }      
    if ($tag == "user_course"){
        $tag5 = true;
    }     
    if ($tag == "course_group"){
        $tag6 = true;
    }     
}?>
<?=Tabs::widget([
    'items' => [
        [
            'label' => "Detail",
            'content' => $this->render('_formDetail', [
                        'model' => $model,
                    ]),
            'active' => $tag1,
        ],
        [
            'label' => 'Keyword',
            'content' => $this->render('_formkeyword', [
                        'model' => $model,
                        'course_id' => $course_id,
                        'courseKeywordDataProvider' => $courseKeywordDataProvider,
                        'searchKeywordModel' => $searchKeywordModel,
                    ]),
            'active' => $tag2,
        ],
        [
            'label' => 'QuestionType',
            'content' => $this->render('_formQuestionType', [
                        'model' => $model,
                        'course_id' => $course_id,
                        'questionTypeDataProvider' => $questionTypeDataProvider,
                        'searchQuestionTypeModel' => $searchQuestionTypeModel,
                    ]),
            'active' => $tag3,                    
        ],
         [
            'label' => 'Question',
            'content' => $this->render('_formQuestion', [
                        'model' => $model,
                        'course_id' => $course_id,
                        'dataQuestionProvider' => $dataQuestionProvider,
                        'searchQuestionModel' => $searchQuestionModel,
                    ]),
            'active' => $tag4,   
        ],
         [
            'label' => 'User',
            'content' => $this->render('_formUser', [
                        'model' => $model,
                        'course_id' => $course_id,
                        'CourseUserProvider' => $CourseUserProvider,
                        'searchCourseUserModel' => $searchCourseUserModel,
                    ]),
            'active' => $tag5,   
        ],     
         [
            'label' => 'Group',
            'content' => $this->render('_formGroup', [
                        'model' => $model,
                        'course_id' => $course_id,
                        'CourseGroupProvider' => $CourseGroupProvider,
                        'searchCourseGroupModel' => $searchCourseGroupModel,
                    ]),
            'active' => $tag6,   
        ],         
    ],
]);?>

