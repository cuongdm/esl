<?php

use backend\models\Coursestatuslookup;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use dosamigos\fileupload\FileUpload;
use dosamigos\fileupload\FileUploadUI;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Courses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-form">

     <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

   <!--div class="field-course_image">
       <label for="course-image" class="control-label">Course_image</label>
       <!?= FileUploadUI::widget([
        'model' => $model,
        'attribute' => 'image',
        'url' => ['courses/upload', 'id' => $model->id],
        'gallery' => false,
        'fieldOptions' => [
            'accept' => 'image/*'
        ],
        'clientOptions' => [
            'maxFileSize' => 6000000
        ],
        // ...
        'clientEvents' => [
            'fileuploaddone' => 'function(e, data) {         
                                    console.log(data.jqXHR.responseJSON.files[0].url);
                                    console.log( $(\'input[type="hidden"][name="Courses[image]"]\'));
                                    $(\'input[type="hidden"][name="Courses[image]"]\').val(data.jqXHR.responseJSON.files[0].url);
                                    console.log($(\'input[type="hidden"][name="Courses[image]"]\').val());
                                }',
            'fileuploadfail' => 'function(e, data) {
                                }',
            'fileuploaddrop' => 'function(e, data) {
                                $(\'input[type="hidden"][name="Course[image]"]\').val("");
                                }',               
        ],
    ]); ?>
    </div-->
    <?= $form->field($model, 'status')->dropDownList(
        ArrayHelper::map(Coursestatuslookup::find()->all(), "id", "name"),
        ["prompt"=>"Select Status"]
    ) ?>
    <?= $form->field($model, 'duration')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
