<?php
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;
$this->title = "Media Management";

echo ElFinder::widget([
    'language'         => 'en',
    'controller'       => 'elfinder',
    'frameOptions'     => [
        'style' => 'width: 100%; height: 500px; border: 0;'    
    ],
    'callbackFunction' => new JsExpression('function(file, id){}')
]);

?>