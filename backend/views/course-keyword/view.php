<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseKeyword */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Course Keywords', 'url' => ['index','tag'=>$tag]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-keyword-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update','tag' => $tag, 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete','tag' => $tag, 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php if ($tag == 'course') { ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'course_id',
                'keyword:ntext',
                'priority',
            ],
        ]) ?>
    <?php } ?>
    <?php if ($tag == 'system') { ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'keyword:ntext',
                'priority',
            ],
        ]) ?>
    <?php } ?>
</div>
