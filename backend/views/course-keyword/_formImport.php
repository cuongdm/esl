<?php

use backend\models\CourseKeyword;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseKeyword */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-keyword-form">

    <?php 
    $form = ActiveForm::begin(); ?>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return ['value' => $model->id];
                }
            ],
            'name:ntext',
            'lvl:ntext'
        ],
    ]);?>
    <div class="form-group">
        <?= Html::submitButton('Import' , ['class' => 'btn btn-success','value'=>'import','name'=>'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
