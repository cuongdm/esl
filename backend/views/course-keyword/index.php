<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Tree;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CourseKeywordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Course Keywords';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-keyword-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
         <?php if ($tag == 'system') {?>
             <?= Html::a('Create Keyword', ['create','tag'=>$tag], ['class' => 'btn btn-success']) ?>
        <?php }?>
        <?php if ($tag == 'course') {?>
             <?= Html::a('Create Course Keyword', ['create','tag'=>$tag], ['class' => 'btn btn-success']) ?>
        <?php } ?>
       
    </p>
    <?php
    use kartik\tree\TreeView;
    echo TreeView::widget([
        // single query fetch to render the tree
        'query'             => Tree::find()->addOrderBy('root, lft'), 
        'headingOptions'    => ['label' => 'Keywords'],
         'rootOptions' => ['label'=>'<span class="text-primary">Keyword</span>'],
        'isAdmin'           => false,                       // optional (toggle to enable admin mode)
        'displayValue'      => 1,                           // initial display value
        //'softDelete'      => true,                        // normally not needed to change
        //'cacheSettings'   => ['enableCache' => true]      // normally not needed to change
    ]);
    ?>

      
</div>
