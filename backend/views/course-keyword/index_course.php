<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Tree;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CourseKeywordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Course Keywords';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-keyword-index">
<?php
use kartik\tree\TreeView;
echo TreeView::widget([
    // single query fetch to render the tree
    'query'             => Tree::find()->addOrderBy('root, lft'), 
    'headingOptions'    => ['label' => 'Keywords'],
     'rootOptions' => ['label'=>'<span class="text-primary">Keyword</span>'],
    'isAdmin'           => false,                       // optional (toggle to enable admin mode)
    'displayValue'      => 1,                           // initial display value
    //'softDelete'      => true,                        // normally not needed to change
    //'cacheSettings'   => ['enableCache' => true]      // normally not needed to change
]);
?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
         <?php if ($tag == 'system') {?>
             <?= Html::a('Create Keyword', ['create','tag'=>$tag], ['class' => 'btn btn-success']) ?>
        <?php }?>
        <?php if ($tag == 'course') {?>
             <?= Html::a('Create Course Keyword', ['create','tag'=>$tag], ['class' => 'btn btn-success']) ?>
        <?php } ?>
       
    </p>

    <?php if ($tag == 'system') {?>
        <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'keyword:ntext',
            'priority',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{updated}{deleted}',
                'buttons' => [
                    'deleted' => function ($url, $model) use ($tag) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete','id'=>$model->id,'tag'=>$tag],[
                            'class' => '',
                            'data' => [
                                'confirm' => 'Bạn có chắc chắn muốn xóa keyword.',
                                'method' => 'post',
                            ]]);
                    },
                    'updated' => function ($url, $model) use ($tag) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id,'tag'=>$tag],[
                            'class' => '',
                            'data' => [
                                'method' => 'post',
                            ]]);
                    }
                ],
            ],
        ],
        ]);?>
    <?php }?>
    <?php if ($tag == 'course') {?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'course_id',
        'keyword:ntext',
        'priority',

        ['class' => 'yii\grid\ActionColumn',
            'template' => '{updated}{deleted}',
            'buttons' => [
                'deleted' => function ($url, $model) use ($tag) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete','id'=>$model->id,'tag'=>$tag],[
                        'class' => '',
                        'data' => [
                            'confirm' => 'Bạn có chắc chắn muốn xóa keyword.',
                            'method' => 'post',
                        ]]);
                },
                'updated' => function ($url, $model) use ($tag) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id,'tag'=>$tag],[
                        'class' => '',
                        'data' => [
                            'method' => 'post',
                        ]]);
                }
            ],
        ],
        ],
        ]);?>
        <?php } ?>
</div>
