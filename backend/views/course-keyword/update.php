<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseKeyword */

$this->title = 'Update Course Keyword: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Course Keywords', 'url' => ['index','tag'=>$tag]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view','tag'=>$tag, 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-keyword-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tag' =>$tag,
    ]) ?>

</div>
