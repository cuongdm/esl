<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseKeyword */

$this->title = 'Create Course Keyword';
if ($tag == 'system'){
    $this->params['breadcrumbs'][] = ['label' => 'Course Keyword', 'url' => ['index','tag'=>$tag]];
}
else {
    $this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['courses/view','tag'=>$tag,'id'=>$model->course_id]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-keyword-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($tag == 'system') { ?>
        <?= $this->render('_form', [
            'model' => $model,
            'tag' => $tag,
        ]); ?>
    <?php } ?>
    <?php if ($tag == 'course') { ?>
        <?= Tabs::widget([
            'items' => [
              /*   [
                    'label' => 'Tạo mới keyword cho khóa học',
                    'content' => $this->render('_form', [
                        'model' => $model,
                        'tag' => $tag,
                    ]),
                    'active' => $createActive
                ], */
                [
                    'label' => 'Import keyword từ hệ thống',
                    'content' => $this->render('_formImport', [
                        'model' => $model,
                        'tag' => $tag,
                        'searchModel'=>$searchModel,
                        'dataProvider'=>$dataProvider,
                    ]),
                    'active'=>$importActive

                ]
            ]
        ]);
        ?>
    <?php } ?>

</div>
