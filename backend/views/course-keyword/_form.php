<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseKeyword */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-keyword-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if ($tag == 'course') { ?>
        <?= $form->field($model, 'course_id')->textInput(['disabled'=>'true']) ?>
    <?php } ?>
    <?= $form->field($model, 'keyword')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'priority')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
            'value'=>$model->isNewRecord ? 'create':'update','name'=>'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
