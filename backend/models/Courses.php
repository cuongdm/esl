<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "courses".
 *
 * @property integer $id
 * @property string $name
 * @property string $price
 * @property string $description
 * @property string $image
 * @property integer $created_at
 * @property double $percent
 * @property int $status
 * @property string $duration
 */
class Courses extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','status'], 'required'],
            [['price', 'percent'], 'number'],
            [['description'], 'string'],
            [['created_at'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 500],
            [['duration'], 'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'description' => 'Description',
            'image' => 'Image',
            'created_at' => 'Created At',
            'status' => 'Status',
            'duration' => 'Duration',

        ];
    }


    public function beforeSave($insert)
    {
        print($this->isNewRecord);
        if($this->isNewRecord)
        {
            $this->created_at = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }
}
