<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $confirmation_token
 * @property integer $status
 * @property integer $superadmin
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $registration_ip
 * @property string $bind_to_ip
 * @property string $email
 * @property integer $email_confirmed
 * @property string $avatarImage
 * @property string $birthday
 * @property string $address
 * @property integer $type
 *
 * @property Messages[] $messages
 * @property Messages[] $messages0
 */
class User extends ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $newPassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash','created_at', 'updated_at', 'avatarImage', 'type'], 'required'],
            ['newPassword', 'required', 'on' => self::SCENARIO_UPDATE],
            [['status', 'superadmin', 'created_at', 'updated_at', 'email_confirmed', 'type'], 'integer'],
            [['birthday'], 'safe'],
            [['address'], 'string'],
            [['username', 'password_hash', 'newPassword','confirmation_token', 'bind_to_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 15],
            [['avatarImage'], 'string', 'max' => 2000],
            ['email', 'trim', 'on' => self::SCENARIO_CREATE],
            ['email', 'required', 'on' => self::SCENARIO_CREATE],
            ['email', 'email'],
            ['email', 'checkDuplication', 'on' => self::SCENARIO_CREATE
            ],
        ];
    }

    public function checkDuplication($attribute, $params, $validator)
    {
        $isExist = $this::find()->where([$attribute=>$this->$attribute])->exists();
        if ($isExist) {
            $this->addError($attribute, 'email exist in system.');
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'newPassword' => 'Password',
            'confirmation_token' => 'Confirmation Token',
            'status' => 'Status',
            'superadmin' => 'Superadmin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'registration_ip' => 'Registration Ip',
            'bind_to_ip' => 'Bind To Ip',
            'email' => 'Email',
            'email_confirmed' => 'Email Confirmed',
            'avatarImage' => 'Avatar Image',
            'birthday' => 'Birthday',
            'address' => 'Address',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['from_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages0()
    {
        return $this->hasMany(Messages::className(), ['whom_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (isset($this->newPassword)) {
            $this->setPassword($this->newPassword);
        }
        return parent::beforeSave($insert);
    }

    public function generatePasswordResetToken()
    {
        $this->confirmation_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->confirmation_token = null;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public static function findByConfirmationToken($token)
    {
        if (!static::isConfirmationTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'confirmation_token' => $token,
            //'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isConfirmationTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
}
