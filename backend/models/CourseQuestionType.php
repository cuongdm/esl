<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "course_question_type".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $question_type_id
 * @property integer $priority
 */
class CourseQuestionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_question_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'question_type_id', 'priority'], 'required'],
            [['course_id', 'question_type_id', 'priority'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'question_type_id' => 'Question Type',
            'priority' => 'Priority',
        ];
    }
}
