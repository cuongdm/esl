<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "question_result".
 *
 * @property integer $id
 * @property integer $question_id
 * @property integer $user_id
 * @property double $result
 * @property integer $created_at
 * @property integer $total_time
 */
class QuestionResult extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','question_id'], 'required'],
            [['user_id', 'created_at', 'total_time'], 'integer'],
            ['result', 'string'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'user_id' => 'User ID',
            'result' => 'Result',
            'created_at' => 'Created At',
            'total_time' => 'Total Time',
        ];
    }
}
