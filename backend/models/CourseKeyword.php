<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "course_keyword".
 *
 * @property integer $id
 * @property integer $course_id
 * @property string $keyword
 * @property integer $priority
 */
class CourseKeyword extends ActiveRecord
{
    /*custom properties*/
    public $keywords;
    const SCENARIO_CREATE = 'create';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'keyword', 'priority'], 'required'],
            [['course_id', 'priority'], 'integer'],
            [['keyword'], 'string'],
            ['keyword', 'checkDuplication', 'on' => self::SCENARIO_CREATE],
        ];
    }

     public function getKeyword()
    {
        return $this->hasOne(Keyword::className(), ['id' => 'keyword']);
    }
    
    public function checkDuplication($attribute, $params, $validator)
    {
        $isExist = $this::find()->where(['keyword'=>$this->keyword,'course_id'=>0])->exists();
        if ($isExist) {
            $this->addError($attribute, 'keyword exist in system.');
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'keyword' => 'Keyword',
            'priority' => 'Priority',
        ];
    }
}
