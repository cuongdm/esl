<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_course".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property string $start_date
 * @property integer $duration
 */
class UserCourse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_course';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id', 'start_date', 'duration'], 'required'],
            [['user_id', 'course_id', 'duration'], 'integer'],
            [['start_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'course_id' => 'Course ID',
            'start_date' => 'Start Date',
            'duration' => 'Duration',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
