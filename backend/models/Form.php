<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "form".
 *
 * @property integer $id
 * @property string $name
 * @property string $email_adress
 * @property string $mobile
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $registration_ip
 * @property string $birthday
 * @property string $address
 * @property integer $course
 * @property integer $password
 */
class Form extends ActiveRecord
{
    const SCENARIO_CREATE = 'create';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email_adress', 'created_at', 'updated_at', 'course'], 'required'],
            [['status' ], 'integer'],
            [['created_at', 'updated_at', 'birthday'], 'safe'],
            [['address','password'], 'string'],
            [['name', 'mobile','course'], 'string', 'max' => 255],
            [['email_adress'], 'string', 'max' => 232],
            [['registration_ip'], 'string', 'max' => 15],
            ['email_adress', 'checkDuplication', 'on' => self::SCENARIO_CREATE ]
            ];
    }

    public function checkDuplication($attribute, $params, $validator)
    {
        $isExist = $this::find()->where([$attribute=>$this->$attribute])->exists();
        if ($isExist) {
            $this->addError($attribute, 'email exist in system.');
            return;
        }
        //check user
        $isExist = User::find()->where(['email'=>$this->$attribute])->exists();
        if ($isExist) {
            $this->addError($attribute, 'email exist in system.');
            return;
        }
    }

    public function getCourses()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email_adress' => 'Email Adress',
            'mobile' => 'Mobile',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'registration_ip' => 'Registration Ip',
            'birthday' => 'Birthday',
            'address' => 'Address',
            'course' => 'Course',
            'password' => 'Password',
        ];
    }
}
