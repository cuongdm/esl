<?php

namespace backend\models;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "coursestatuslookup".
 *
 * @property integer $id
 * @property string $name
 */
class Coursestatuslookup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coursestatuslookup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
