<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Question;
use backend\models\Keyword;

/**
 * QuestionSearch represents the model behind the search form about `backend\models\Question`.
 */
class QuestionSearch extends Question
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id'], 'integer'],
            [['type', 'keyword', 'question_text', 'question_audio', 'question_img', 'question_video', 'answer_text', 'answer_img', 'hint', 'description', 'time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Question::find()->select(['question.*', 'course_keyword.priority as priority'])
        ->leftJoin('course_keyword', 'question.keyword = course_keyword.keyword')
        ->orderBy(['priority' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['type'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'time' => $this->time,
            'question.course_id' => $this->course_id,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'keyword', $this->keyword])
            ->andFilterWhere(['like', 'question_text', $this->question_text])
            ->andFilterWhere(['like', 'question_audio', $this->question_audio])
            ->andFilterWhere(['like', 'question_img', $this->question_img])
            ->andFilterWhere(['like', 'question_video', $this->question_video])
            ->andFilterWhere(['like', 'answer_text', $this->answer_text])
            ->andFilterWhere(['like', 'answer_img', $this->answer_img])
            ->andFilterWhere(['like', 'hint', $this->hint])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function searchQuestionSystem($params)
    {
        $sql = "SELECT * FROM question WHERE (type, keyword) NOT IN ( SELECT DISTINCT type, keyword FROM question where course_id = :id) and course_id = 0";
        $this->load($params);         
        $query = Question::findBySql($sql,[':id'=>$this->course_id]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
