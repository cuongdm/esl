<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "course_group".
 *
 * @property integer $id
 * @property string $name
 * @property integer $course_id
 */
class CourseGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', ], 'required'],
            [['course_id'], 'integer'],
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'course_id' => 'Course ID',
        ];
    }
}
