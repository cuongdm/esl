<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CourseKeyword;
use backend\models\Keyword;

/**
 * CourseKeywordSearch represents the model behind the search form about `backend\models\CourseKeyword`.
 */
class CourseKeywordSearch extends CourseKeyword
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'priority'], 'integer'],
            [['keyword'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseKeyword::find()
            ->select(['course_keyword.id','course_keyword.priority', 'course_keyword.keyword as keyword_id','keyword.name as keyword'])
            ->innerJoinWith('keyword', '`keyword`.`id`=`course_keyword`.`keyword` ');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'priority' => $this->priority,
        ]);

        $query->andFilterWhere(['like', 'keyword', $this->keyword]);

        return $dataProvider;
    }

    public function searchKeywordSystem($params)
    {
        $query = CourseKeyword::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'keyword', $this->keyword]);
        $query->andWhere('course_id = 0');
        $query->andWhere('keyword not in (select keyword from course_keyword where course_id ='. $this->course_id . ')');
        return $dataProvider;
    }
    
        public function searchAllKeyword($params)
    {
        $query = Keyword::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //$query->andFilterWhere(['like', 'keyword', $this->keyword]);
        $query->andWhere('active = 1');
        $query->andWhere('lvl = 2');
        $query->andWhere('id not in (select keyword from course_keyword where course_id ='. $this->course_id . ')');
        return $dataProvider;
    }
}
