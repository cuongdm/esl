<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "question_type".
 *
 * @property integer $id
 * @property string $type
 * @property string $type_name
 */
class QuestionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'type_name'], 'required'],
            [['id'], 'integer'],
            [['type'], 'string', 'max' => 100],
            [['type_name'], 'string', 'max' => 250],
            [['icon'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'type_name' => 'Type Name',
            'icon' => 'Icon',
        ];
    }
}
