<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "question".
 *
 * @property integer $id
 * @property string $type
 * @property string $keyword
 * @property string $question_text
 * @property string $question_audio
 * @property string $question_img
 * @property string $question_video
 * @property string $answer_text
 * @property string $answer_img
 * @property string $hint
 * @property string $description
 * @property string $time
 * @property integer $course_id
 */
class Question extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'keyword'], 'required'],
            [['keyword', 'question_text', 'question_audio', 'question_img', 'question_video', 'answer_text', 'answer_img', 'hint', 'description'], 'string'],
            [['time'], 'safe'],
            [['course_id'], 'integer'],
            [['type'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'keyword' => 'Keyword',
            'question_text' => 'Question Text',
            'question_audio' => 'Question Audio',
            'question_img' => 'Question Img',
            'question_video' => 'Question Video',
            'answer_text' => 'Answer Text',
            'answer_img' => 'Answer Img',
            'hint' => 'Hint',
            'description' => 'Description',
            'time' => 'Time',
            'course_id' => 'Course ID',
        ];
    }
}
