<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "keyword".
 *
 * @property string $id
 * @property integer $root
 * @property integer $lvl
 * @property string $name
 * @property string $icon
 */
class Keyword extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root', 'lvl', ], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['icon'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root' => 'Root',
            'lvl' => 'Level',
            'name' => 'Name',
            'icon' => 'Description',
        ];
    }
}
