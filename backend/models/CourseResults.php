<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "course_results".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $score
 * @property integer $status
 */
class CourseResults extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['score'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'score' => 'Score',
            'status' => 'Status',
        ];
    }
}
