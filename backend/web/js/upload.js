jQuery(document).ready(function () {
    var hide_val = {"1":["question_audio","question_image","answer_text","answer_img","hint","description","time"],
        "2":["question_audio","question_image","question_video","answer_text","answer_img","hint","description","time"],
        "3":["question_video","answer_img","hint","answer_text","description","time"],
        "4":["question_video","answer_img","hint","description","time"],
        "5":["question_image","question_video","answer_text","answer_img","hint","description","time"],
        "6":["question_image","question_video","answer_text","answer_img","hint","description","time"],
        "7":["question_image","question_video","answer_img","hint","description","time"],
        "8":["question_audio","question_image","question_video","answer_text","answer_img","hint","description","time"],
        "9":["question_audio","question_image","question_video","answer_text","answer_img","hint","description","time"],
    };
    
    function hide_input(val){
        for (i in hide_val) { 
            for (id in (hide_val[i])){
                 $(".field-question-"+hide_val[i][id]).show();
             };
        }
         for (id in (hide_val[val])){
             $(".field-question-"+hide_val[val][id]).hide();
         };
    }
    if(typeof($("#question-type").val())!='undefined' && $("#question-type").val()!=""){
        hide_input($("#question-type").val());
    };
    $("#question-type").on("change", function(event) { 
        hide_input(this.value);
    } );
    $(".custom_button").on("click", function(event) { 
        console.log(this.value);
        $.get( this.value , function( data ) {
          $("#group-user").html(data);
        });
    } );    
});

