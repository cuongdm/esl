<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\jui\\' => array($vendorDir . '/yiisoft/yii2-jui'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'webvimark\\modules\\UserManagement\\' => array($vendorDir . '/webvimark/module-user-management'),
    'webvimark\\image\\' => array($vendorDir . '/webvimark/image'),
    'webvimark\\helpers\\' => array($vendorDir . '/webvimark/helpers'),
    'webvimark\\extensions\\GridPageSize\\' => array($vendorDir . '/webvimark/grid-page-size'),
    'webvimark\\extensions\\GridBulkActions\\' => array($vendorDir . '/webvimark/grid-bulk-actions'),
    'webvimark\\extensions\\DateRangePicker\\' => array($vendorDir . '/webvimark/date-range-picker'),
    'webvimark\\extensions\\BootstrapSwitch\\' => array($vendorDir . '/webvimark/bootstrap-switch'),
    'webvimark\\components\\' => array($vendorDir . '/webvimark/components'),
    'mihaildev\\elfinder\\' => array($vendorDir . '/mihaildev/yii2-elfinder'),
    'mihaildev\\ckeditor\\' => array($vendorDir . '/mihaildev/yii2-ckeditor'),
    'kartik\\tree\\' => array($vendorDir . '/kartik-v/yii2-tree-manager'),
    'kartik\\plugins\\fileinput\\' => array($vendorDir . '/kartik-v/bootstrap-fileinput'),
    'kartik\\form\\' => array($vendorDir . '/kartik-v/yii2-widget-activeform'),
    'kartik\\file\\' => array($vendorDir . '/kartik-v/yii2-widget-fileinput'),
    'kartik\\dialog\\' => array($vendorDir . '/kartik-v/yii2-dialog'),
    'kartik\\base\\' => array($vendorDir . '/kartik-v/yii2-krajee-base'),
    'dosamigos\\gallery\\' => array($vendorDir . '/2amigos/yii2-gallery-widget/src'),
    'dosamigos\\fileupload\\' => array($vendorDir . '/2amigos/yii2-file-upload-widget/src'),
    'creocoder\\nestedsets\\' => array($vendorDir . '/creocoder/yii2-nested-sets/src'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'abhimanyu\\sms\\' => array($vendorDir . '/abhi1693/yii2-sms'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Stecman\\Component\\Symfony\\Console\\BashCompletion\\' => array($vendorDir . '/stecman/symfony-console-completion/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Codeception\\Extension\\' => array($vendorDir . '/codeception/base/ext'),
    'Codeception\\' => array($vendorDir . '/codeception/base/src/Codeception'),
);
